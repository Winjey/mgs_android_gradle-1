package com.ms.mygaragesale.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.ui.frag.AuthenticationFragment;

public class AuthenticationActivity extends ActionBarActivity {
	
	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		this.setContentView(R.layout.activity_authentication);
		this.initialize();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		Fragment fragment = getFragmentManager().findFragmentById(R.id.layout_container);
		fragment.onActivityResult(requestCode, resultCode, data);
	}
	
	private void initialize() {		
		FragmentManager fragmentManager = getFragmentManager();
		Fragment fragment = fragmentManager.findFragmentById(R.id.layout_container);
		if (fragment == null) {
			fragment = AuthenticationFragment.newInstance();
			FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
			fragmentTransaction.replace(R.id.layout_container, fragment);
			fragmentTransaction.commit();
		}
	}
	
}
