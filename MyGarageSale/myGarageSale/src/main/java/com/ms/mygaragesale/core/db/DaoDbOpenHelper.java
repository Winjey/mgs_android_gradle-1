package com.ms.mygaragesale.core.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

import com.ms.mygaragesale.core.db.dao.AlertKeywordTable;
import com.ms.mygaragesale.core.db.dao.AlertKeywordTableDao;
import com.ms.mygaragesale.core.db.dao.CategoryTableDao;
import com.ms.mygaragesale.core.db.dao.DaoMaster;
import com.ms.mygaragesale.core.db.dao.DaoMaster.DevOpenHelper;

public class DaoDbOpenHelper extends DevOpenHelper {
	
	private static final String DATABASE_NAME = "MyGarageSale.db";
	
	public DaoDbOpenHelper(Context context) {
		super(context, DATABASE_NAME, null);
	}

	public DaoDbOpenHelper(Context context, String name, CursorFactory factory) {
		super(context, name, factory);
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		super.onCreate(db);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		DbUpgradeManager.upgrade(db, oldVersion, newVersion);
	}


	private static class DbUpgradeManager {

		public static void upgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			if (newVersion > oldVersion) {
				switch (oldVersion) {
					case 1:
						DbUpgradeManager.upgradeFrom1to2(db);
						DbUpgradeManager.upgradeFrom2to3(db);
						break;

					case 2:
						DbUpgradeManager.upgradeFrom2to3(db);
						break;
				}
			}
		}

		private static void upgradeFrom1to2(SQLiteDatabase db) {
			// drop all tables
			DaoMaster.dropAllTables(db, true);
			// create all tables
			DaoMaster.createAllTables(db, true);
		}

		private static void upgradeFrom2to3(SQLiteDatabase db) {
			// drop category table
			CategoryTableDao.dropTable(db, true);
			// add alert keyword table
			AlertKeywordTableDao.createTable(db, true);
		}

	}

}
