package com.ms.mygaragesale.core.db;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.db.dao.AlertKeywordTable;
import com.ms.mygaragesale.core.db.dao.AlertKeywordTableDao;
import com.ms.mygaragesale.core.db.dao.CurrentUserTable;
import com.ms.mygaragesale.core.db.dao.DaoMaster;
import com.ms.mygaragesale.core.db.dao.DaoMaster.DevOpenHelper;
import com.ms.mygaragesale.core.db.dao.DaoSession;
import com.ms.mygaragesale.core.db.dao.SearchTable;
import com.ms.mygaragesale.core.db.dao.SearchTableDao;
import com.ms.mygaragesale.model.AlertKeyword;
import com.ms.mygaragesale.model.CurrentUser;

public class DatabaseProvider {

	private static DatabaseProvider databaseProvider;
	
	private SQLiteDatabase sqliteDatabase;
	private Context appContext;
	private DaoSession daoSession;
	private DaoMaster daoMaster;
	
	private DaoSession getDaoSession() {
		if (daoSession == null) {
			daoSession = getDaoMaster().newSession();
		}
		return daoSession;
	}
	
	private DaoMaster getDaoMaster() {
		if (daoMaster == null) {
			DevOpenHelper helper = new DaoDbOpenHelper(appContext);
			sqliteDatabase = helper.getWritableDatabase();
			daoMaster = new DaoMaster(sqliteDatabase);
		}
		return daoMaster;
	}
	
	private DatabaseProvider(Context context) {
		appContext = context.getApplicationContext();
	}
	
	public static DatabaseProvider getInstance() {
		if (databaseProvider == null) {
			Context context = MGSApplication.getInstance().getApplicationContext();
			databaseProvider = new DatabaseProvider(context);
		}
		return databaseProvider;
	}
	
//	public void loadUser(User user) {
//		String query = "SELECT * FROM " + DBConst.TableName.USER;
//		Cursor cursor = null;
//		try {
//			cursor = this.sqliteDatabase.rawQuery(query, null);
//			if (cursor != null && cursor.getCount() > 0) {
//				cursor.moveToFirst();
//				user.setId(cursor.getLong(cursor.getColumnIndex(DBConst.ColumnName.ID)));
//				user.setUsername(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.USERNAME)));
//				user.setPassword(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.PASSWORD)));
//				user.setName(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.NAME)));
//				user.setEmailId(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.EMAIL_ID)));
//				user.setMobileNumber(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.MOBILE_NUMBER)));
//				user.setUserRole(cursor.getInt(cursor.getColumnIndex(DBConst.ColumnName.USER_ROLE)));
//				user.setAccountStatus(cursor.getInt(cursor.getColumnIndex(DBConst.ColumnName.ACCOUNT_STATUS)));
//				user.setAddress(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.ADDRESS)));
//				user.setLocation(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.LOCATION)));
//				user.setAccountType(cursor.getInt(cursor.getColumnIndex(DBConst.ColumnName.ACCOUNT_TYPE)));
//				user.setImageId(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.IMAGE_ID)));
//			} else {
//				insertUser(user);
//			}
//		} catch (Exception e) {
//			if (Logger.DEBUG) {
//				e.printStackTrace();
//			}
//		} finally {
//			if (cursor != null) {
//				cursor.close();
//			}
//		}	
		
//	}

	public void deleteAll() {
		try {
			DaoSession session = getDaoSession();
			session.getAlertKeywordTableDao().deleteAll();
			session.getSearchTableDao().deleteAll();
			session.getCurrentUserTableDao().deleteAll();
			session.getUserImageTableDao().deleteAll();
			session.getUserLocationTableDao().deleteAll();
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public CurrentUser getCurrentUser() {
		CurrentUser currentUser = null;
		try {
			long count = getDaoSession().getCurrentUserTableDao().count();
			if (count == 0) {
				currentUser = new CurrentUser();
				currentUser.setId(new AppSharedPref().getLongValue());
				currentUser.setDataSyncd(true);
				currentUser.setUserRole(CurrentUser.USER_ROLE_USER);
				insertCurrentUser(currentUser);
			} else {
				CurrentUserTable dbCurrentUser = getDaoSession().getCurrentUserTableDao().loadAll().get(0);
				currentUser = new CurrentUser(dbCurrentUser);
			}
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return currentUser;
	}
	
	public void insertCurrentUser(CurrentUser currentUser) {
		try {
			CurrentUserTable dbCurrentUser = currentUser.getDbCurrentUser();
			getDaoSession().getCurrentUserTableDao().insert(dbCurrentUser);
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public void updateCurrentUser(CurrentUser currentUser) {
		try {
			CurrentUserTable dbCurrentUser = currentUser.getDbCurrentUser();
			getDaoSession().getCurrentUserTableDao().update(dbCurrentUser);
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public void deleteCurrentUser(CurrentUser currentUser) {
		try {
			CurrentUserTable dbCurrentUser = currentUser.getDbCurrentUser();
			getDaoSession().getCurrentUserTableDao().delete(dbCurrentUser);
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<AlertKeyword> getAlertKeywords() {
		ArrayList<AlertKeyword> alertKeywords = new ArrayList<>();
		try {
			List<AlertKeywordTable> alertKeywordTables = getDaoSession().getAlertKeywordTableDao().loadAll();
			for (AlertKeywordTable alertKeywordTable : alertKeywordTables) {
				alertKeywords.add(new AlertKeyword(alertKeywordTable));
			}
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return alertKeywords;
	}

	public ArrayList<AlertKeyword> getAlertKeywords(int keywordType) {
		ArrayList<AlertKeyword> alertKeywords = new ArrayList<>();
		try {
			List<AlertKeywordTable> alertKeywordTables = getDaoSession().getAlertKeywordTableDao().queryBuilder().where(AlertKeywordTableDao.Properties.KeywordType.eq(keywordType)).list();
			for (AlertKeywordTable alertKeywordTable : alertKeywordTables) {
				alertKeywords.add(new AlertKeyword(alertKeywordTable));
			}
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return alertKeywords;
	}

	public void updateAlertKeywords(ArrayList<AlertKeyword> alertKeywords) {
		try {
			AlertKeywordTableDao tableDao = getDaoSession().getAlertKeywordTableDao();
			// delete all alert keywords
			tableDao.deleteAll();

			// insert new alert keywords
			AlertKeywordTable[] alertKeywordTables = new AlertKeywordTable[alertKeywords.size()];
			for (int i = 0; i < alertKeywords.size(); i++) {
				alertKeywordTables[i] = alertKeywords.get(i).getAlertKeywordTable();
			}
			tableDao.insertInTx(alertKeywordTables);
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public void addAlertKeyword(AlertKeyword alertKeyword) {
		try {
			AlertKeywordTable alertKeywordTable = alertKeyword.getAlertKeywordTable();
			getDaoSession().getAlertKeywordTableDao().insert(alertKeywordTable);
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}

	public void addAlertKeywords(ArrayList<AlertKeyword> alertKeywords) {
		try {
			for (AlertKeyword alertKeyword : alertKeywords) {
				this.addAlertKeyword(alertKeyword);
			}
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public void deleteAlertKeywords(ArrayList<AlertKeyword> alertKeywords) {
		try {
			List<AlertKeywordTable> alertKeywordTables = new ArrayList<>(alertKeywords.size());
			for (AlertKeyword alertKeyword : alertKeywords) {
				alertKeywordTables.add(alertKeyword.getAlertKeywordTable());
			}
			AlertKeywordTableDao tableDao = getDaoSession().getAlertKeywordTableDao();
			tableDao.deleteInTx(alertKeywordTables);
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}

	public void deleteAlertKeyword(AlertKeyword alertKeyword) {
		try {
			AlertKeywordTable alertKeywordTable = alertKeyword.getAlertKeywordTable();
			AlertKeywordTableDao tableDao = getDaoSession().getAlertKeywordTableDao();
			tableDao.delete(alertKeywordTable);
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public ArrayList<String> getSearchedKeywords() {
		ArrayList<String> keywords = new ArrayList<>();
		try {
			List<SearchTable> searchTables = getDaoSession().getSearchTableDao().loadAll();
			if (searchTables != null) {
				for (SearchTable searchTable : searchTables) {
					keywords.add(searchTable.getQueryString());
				}
			}
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return keywords;
	}

	public void deleteSearchedKeywords() {
		try {
			getDaoSession().getSearchTableDao().deleteAll();
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	public void addSearchKeyword(String keyword) {
		try {
			if (!isSearchKeywordExists(keyword)) {
				SearchTable searchTable = new SearchTable();
				searchTable.setQueryString(keyword);
				getDaoSession().getSearchTableDao().insert(searchTable);
			}
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean isSearchKeywordExists(String keyword) {
		try {
			List<SearchTable> searchTableList = getDaoSession().getSearchTableDao().queryBuilder().where(SearchTableDao.Properties.QueryString.eq(keyword)).list();
			if (searchTableList != null && !searchTableList.isEmpty()) {
				return true;
			}
		} catch (Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return false;
	}
		
//	public SaleItem getSaleItem() {
//		User user = null;
//		long count = getDaoSession().getUserDao().count();
//		if (count == 0) {
//			user = new User();
//			insertUser(user);
//		} else {
//			com.ms.mygaragesale.core.db.dao.User dbUser = getDaoSession().getUserDao().loadAll().get(0);
//			user = new User(dbUser);
//		}
//		return user;
//	}
	
//	public void insertSaleItem(SaleItem saleItem) {
//		com.ms.mygaragesale.core.db.dao.SaleItemTable dbSaleItem = saleItem.getDbSaleItem();
//		getDaoSession().getSaleItemTableDao().insert(dbSaleItem);
//	}
//	
//	public void updateSaleItem(SaleItem saleItem) {
//		com.ms.mygaragesale.core.db.dao.SaleItemTable dbSaleItem = saleItem.getDbSaleItem();
//		getDaoSession().getSaleItemTableDao().update(dbSaleItem);
//	}
//	
//	public SaleItem getSaleItem(long saleItemId) {
//		SaleItemTable saleItemTable = getDaoSession().getSaleItemTableDao().load(saleItemId);
//		return new SaleItem(saleItemTable);
//	}
//
//	public void insertUser(User user) {
//		com.ms.mygaragesale.core.db.dao.UserTable dbUser = user.getDbUser();
//		getDaoSession().getUserTableDao().insert(dbUser);
//	}
//	
//	public void updateUser(User user) {
//		com.ms.mygaragesale.core.db.dao.UserTable dbUser = user.getDbUser();
//		getDaoSession().getUserTableDao().update(dbUser);
//	}
//	
//	public void insertSaleItemImage(SaleItemImage saleItemImage) {
//		SaleItemImageTable dbSaleItemImage = saleItemImage.getDbSaleItemImage();
//		getDaoSession().getSaleItemImageTableDao().insert(dbSaleItemImage);
//	}
//	
//	public void updateSaleItemImage(SaleItemImage saleItemImage) {
//		SaleItemImageTable dbSaleItemImage = saleItemImage.getDbSaleItemImage();
//		getDaoSession().getSaleItemImageTableDao().update(dbSaleItemImage);
//	}
//	
//	public void insertSaleItemImages(List<SaleItemImage> saleItemImages) {
//		for (SaleItemImage saleItemImage : saleItemImages) {
//			insertSaleItemImage(saleItemImage);
//		}
//	}
//	
//	public void updateSaleItemImages(List<SaleItemImage> saleItemImages) {
//		for (SaleItemImage saleItemImage : saleItemImages) {
//			updateSaleItemImage(saleItemImage);
//		}
//	}
	
//	public void insertSaleItems(ArrayList<SaleItem> saleItems) {
//		SaleItemTable saleItemTable = new SaleItemTable();
//		getDaoSession().getSaleItemTableDao().delete(entity)
//	}
	
//	public void saveUser(User user) {
//		this.updateUser(user);
//	}
	
//	public void insertUser(User user) {
//		ContentValues contentValues = new ContentValues();
//		contentValues.put(DBConst.ColumnName.ID, user.getId());
//		contentValues.put(DBConst.ColumnName.USERNAME, user.getUsername());
//		contentValues.put(DBConst.ColumnName.PASSWORD, user.getPassword());
//		contentValues.put(DBConst.ColumnName.NAME, user.getName());
//		contentValues.put(DBConst.ColumnName.EMAIL_ID, user.getEmailId());
//		contentValues.put(DBConst.ColumnName.MOBILE_NUMBER, user.getMobileNumber());
//		contentValues.put(DBConst.ColumnName.USER_ROLE, user.getUserRole());
//		contentValues.put(DBConst.ColumnName.ACCOUNT_STATUS, user.getAccountStatus());
//		contentValues.put(DBConst.ColumnName.ADDRESS, user.getAddress());
//		contentValues.put(DBConst.ColumnName.LOCATION, user.getLocation());
//		contentValues.put(DBConst.ColumnName.ACCOUNT_TYPE, user.getAccountType());
//		contentValues.put(DBConst.ColumnName.ACCESS_TOKEN, user.getAccessToken());
//		contentValues.put(DBConst.ColumnName.IMAGE_ID, user.getImageId());
//		try {
//			long id = this.sqliteDatabase.insert(DBConst.TableName.USER, null, contentValues);
//			Logger.log("row id: " + id + " inserted.");
//		} catch (Exception e) {
//			if (Logger.DEBUG) {
//				e.printStackTrace();
//			}
//		} 
//	}
//	
//	public void updateUser(User user) {
//		ContentValues contentValues = new ContentValues();
//		contentValues.put(DBConst.ColumnName.ID, user.getId());
//		contentValues.put(DBConst.ColumnName.USERNAME, user.getUsername());
//		contentValues.put(DBConst.ColumnName.PASSWORD, user.getPassword());
//		contentValues.put(DBConst.ColumnName.NAME, user.getName());
//		contentValues.put(DBConst.ColumnName.EMAIL_ID, user.getEmailId());
//		contentValues.put(DBConst.ColumnName.MOBILE_NUMBER, user.getMobileNumber());
//		contentValues.put(DBConst.ColumnName.USER_ROLE, user.getUserRole());
//		contentValues.put(DBConst.ColumnName.ACCOUNT_STATUS, user.getAccountStatus());
//		contentValues.put(DBConst.ColumnName.ADDRESS, user.getAddress());
//		contentValues.put(DBConst.ColumnName.LOCATION, user.getLocation());
//		contentValues.put(DBConst.ColumnName.ACCOUNT_TYPE, user.getAccountType());
//		contentValues.put(DBConst.ColumnName.ACCESS_TOKEN, user.getAccessToken());
//		contentValues.put(DBConst.ColumnName.IMAGE_ID, user.getImageId());
//		try {
//			int i = this.sqliteDatabase.update(DBConst.TableName.USER, contentValues, null, null);
//			Logger.log( i + " rows affected.");
//		} catch (Exception e) {
//			if (Logger.DEBUG) {
//				e.printStackTrace();
//			}
//		} 
//	}
//	
//	
//	public LocalImage getLocalImage(String imageId) {
//		String query = "SELECT * FROM " + DBConst.TableName.IMAGE + " WHERE " + DBConst.ColumnName.ID + "=" + imageId;
//		LocalImage localImage = null;
//		Cursor cursor = null;
//		try {
//			cursor = this.sqliteDatabase.rawQuery(query, null);
//			if (cursor != null && cursor.getCount() > 0) {
//				cursor.moveToFirst();
//				localImage = new LocalImage();
//				localImage.setId(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.ID)));
//				localImage.setImageUri(cursor.getString(cursor.getColumnIndex(DBConst.ColumnName.IMAGE_URI)));
//			} 
//		} catch (Exception e) {
//			if (Logger.DEBUG) {
//				e.printStackTrace();
//			}
//		} finally {
//			if (cursor != null) {
//				cursor.close();
//			}
//		}	
//		return localImage;
//	}
//		
//	public void addLocalImage(LocalImage image) {
//		ContentValues contentValues = new ContentValues();
//		contentValues.put(DBConst.ColumnName.ID, image.getId());
//		contentValues.put(DBConst.ColumnName.IMAGE_URI, image.getImageUri());
//		try {
//			long id = this.sqliteDatabase.insert(DBConst.TableName.IMAGE, null, contentValues);
//			Logger.log("row id: " + id + " inserted.");
//		} catch (Exception e) {
//			if (Logger.DEBUG) {
//				e.printStackTrace();
//			}
//		} 
//	}
//	
//	public void addLocalImages(ArrayList<LocalImage> localImages) {
//		for (LocalImage localImage : localImages) {
//			this.addLocalImage(localImage);
//		}
//	}
//	
//	public void deleteLocalImage(String imageId) {
// 
//	}
//	
//	public void deleteLocalImages(ArrayList<LocalImage> localImages) {
//		for (LocalImage localImage : localImages) {
//			deleteLocalImage(localImage.getId());
//		}
//	}
//	
//	public void insertSaleItem(SaleItem saleItem) {
//		ContentValues contentValues = new ContentValues();
//		contentValues.put(DBConst.ColumnName.ID, saleItem.getId());
//		contentValues.put(DBConst.ColumnName.DESCRIPTION, saleItem.getDescription());
//		contentValues.put(DBConst.ColumnName.IMAGES_ONE, saleItem.getImageOne());
//		contentValues.put(DBConst.ColumnName.IMAGES_TWO, saleItem.getImageTwo());
//		contentValues.put(DBConst.ColumnName.IMAGES_THREE, saleItem.getImageThree());
//		contentValues.put(DBConst.ColumnName.MESSAGE_ID, saleItem.getMessagesId());
//		contentValues.put(DBConst.ColumnName.TITLE, saleItem.getTitle());
//		contentValues.put(DBConst.ColumnName.MESSAGE_COUNT, saleItem.getMessageCount());
//		contentValues.put(DBConst.ColumnName.PRICE, saleItem.getPrice());
//		contentValues.put(DBConst.ColumnName.STATUS, saleItem.getStatus());
//		contentValues.put(DBConst.ColumnName.CREATED_DATE, saleItem.getCreatedDate().toString());
//		contentValues.put(DBConst.ColumnName.MODIFIED_DATE, saleItem.getModifeidDate().toString());
//		contentValues.put(DBConst.ColumnName.USER_ID, saleItem.getSeller().getId());
//		try {
//			long id = this.sqliteDatabase.insert(DBConst.TableName.SALE_ITEM, null, contentValues);
//			Logger.log("row id: " + id + " inserted.");
//		} catch (Exception e) {
//			if (Logger.DEBUG) {
//				e.printStackTrace();
//			}
//		} 
//	}
//	
//	public void updateSaleItem(SaleItem saleItem) {
//		ContentValues contentValues = new ContentValues();
//		contentValues.put(DBConst.ColumnName.ID, saleItem.getId());
//		contentValues.put(DBConst.ColumnName.DESCRIPTION, saleItem.getDescription());
//		contentValues.put(DBConst.ColumnName.IMAGES_ONE, saleItem.getImageOne());
//		contentValues.put(DBConst.ColumnName.IMAGES_TWO, saleItem.getImageTwo());
//		contentValues.put(DBConst.ColumnName.IMAGES_THREE, saleItem.getImageThree());
//		contentValues.put(DBConst.ColumnName.MESSAGE_ID, saleItem.getMessagesId());
//		contentValues.put(DBConst.ColumnName.TITLE, saleItem.getTitle());
//		contentValues.put(DBConst.ColumnName.MESSAGE_COUNT, saleItem.getMessageCount());
//		contentValues.put(DBConst.ColumnName.PRICE, saleItem.getPrice());
//		contentValues.put(DBConst.ColumnName.STATUS, saleItem.getStatus());
//		contentValues.put(DBConst.ColumnName.CREATED_DATE, saleItem.getCreatedDate().toString());
//		contentValues.put(DBConst.ColumnName.MODIFIED_DATE, saleItem.getModifeidDate().toString());
//		contentValues.put(DBConst.ColumnName.USER_ID, saleItem.getSeller().getId());
//		try {
//			String[] whereArgs = { "" + saleItem.getId() }; 
//			long id = this.sqliteDatabase.update(DBConst.TableName.SALE_ITEM, contentValues, "id=?", whereArgs);
//			Logger.log("row id: " + id + " inserted.");
//		} catch (Exception e) {
//			if (Logger.DEBUG) {
//				e.printStackTrace();
//			}
//		} 
//	}

}
