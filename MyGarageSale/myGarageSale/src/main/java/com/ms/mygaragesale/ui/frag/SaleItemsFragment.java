package com.ms.mygaragesale.ui.frag;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.etsy.android.grid.StaggeredGridView;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.response.SaleItemsResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;
import com.ms.mygaragesale.ui.MainActivity;
import com.ms.mygaragesale.ui.SearchActivity;

public class SaleItemsFragment extends RefreshableItemsWithCountHeaderFragment<SaleItem> implements OnClickListener {

	public static final String KEY_SALE_ITEM_TYPE = "saleItemType";
	public static final int REQUEST_CODE_SALE_ITEM_DETAIL = 1001;
	
	protected ApiTask<SaleItemsResponse> saleItemApiTask;
	
	private ArrayList<SaleItem> allSaleItemsList;
	protected HashSet<Integer> hashSetForOneTimeAnimation;
	protected SaleItemViewType saleItemViewType;
	private boolean[] filteredItems = {true, true, true, true};
	private boolean isSaleItemByMeOnly;

	public static SaleItemsFragment newInstance(SaleItemViewType saleItemViewType) {
		final SaleItemsFragment fragment = new SaleItemsFragment();
		Bundle bundle = new Bundle();
		bundle.putInt(KEY_SALE_ITEM_TYPE, saleItemViewType.getValue());
		fragment.setArguments(bundle);
		return fragment;
	}
	
	private boolean isCreatedByMe() {
		if (saleItemViewType == SaleItemViewType.MY_OPEN_SALE_ITEM
			|| saleItemViewType == SaleItemViewType.MY_PENDING_SALE_ITEM
				|| saleItemViewType == SaleItemViewType.MY_REJECTED_SALE_ITEM
					|| saleItemViewType == SaleItemViewType.MY_SOLD_SALE_ITEM
						|| saleItemViewType == SaleItemViewType.MY_SALE_ITEM) {
			return true;
		}
		return false;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRetainInstance(true);
		hashSetForOneTimeAnimation = new HashSet<>();
		hashSetForOneTimeAnimation.add(0);
		allSaleItemsList = new ArrayList<>();
		
		saleItemViewType = SaleItemViewType.ALL_POSTS;
		this.fetchArguments();
		
		// refresh data from API
		getSaleItemFromApi(true);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		NavigationDrawerFragment navigationDrawerFragment = null;
		if (getActivity() instanceof MainActivity) {
			navigationDrawerFragment = ((MainActivity) getActivity()).getNavigationDrawerFragment();
		}
		if (navigationDrawerFragment == null || !navigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			inflater.inflate(R.menu.my_sale_items, menu);
			if (!isSaleItemByMeOnly) {
				menu.removeItem(R.id.action_filter);
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		boolean result = super.onOptionsItemSelected(item);
		if (item.getItemId() == R.id.action_search) {
			Intent intent = new Intent(getActivity(), SearchActivity.class);
			startActivity(intent);
			getActivity().overridePendingTransition(0, 0);
			result = true;
		} else if (item.getItemId() == R.id.action_filter) {
			this.filterItems();
			result = true;
		}
		return result;
	}
		
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == REQUEST_CODE_SALE_ITEM_DETAIL && resultCode == Activity.RESULT_OK) {
			getSaleItemFromApi(false);
		}
	}
	
	private boolean isNeedToRefresh() {
		boolean isNeedForRefresh = false;
		long lastUpdatedTime = getLastUpdatedTime();
		Date dateNow = new Date();
		Date lastUpdatedDate = new Date(lastUpdatedTime);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(dateNow);
		calendar.add(Calendar.DAY_OF_YEAR, -1);
        Date yesterdayDate = calendar.getTime();
		if (yesterdayDate.after(lastUpdatedDate)) {
			isNeedForRefresh = true;
		}
		return isNeedForRefresh;
	}
	
	@Override
	protected long getLastUpdatedTime() {
		long lastUpdatedTime = 0;
		lastUpdatedTime = new AppSharedPref().getSaleItemsLastUpdatedTime(saleItemViewType);	
		return lastUpdatedTime;
	}

	protected void fetchArguments() {
		Bundle bundle = getArguments();
		if (bundle != null && bundle.containsKey(KEY_SALE_ITEM_TYPE)) {
			saleItemViewType = SaleItemViewType.getSaleItemViewType(bundle.getInt(KEY_SALE_ITEM_TYPE));
			isSaleItemByMeOnly = isCreatedByMe();
		}
	}
	
	private void setLastUpdatedTime(long time) {
		new AppSharedPref().setSaleItemsLastUpdatedTime(saleItemViewType, time);
	}
	
	protected void getSaleItemFromApi(boolean shouldResultCacheResponse) {
		String url = null;
		if (isSaleItemByMeOnly) {
			url = UrlUtil.getSaleItemsByMeUrl(CurrentUser.getCurrentUser().getUserId(), getSaleItemStatusBasedOnViewType());
		} else {
			int sponsored = 0;
			if (saleItemViewType == SaleItemViewType.SPONSORED_SALE_ITEM) {
				sponsored = 1;
			}
			url = UrlUtil.getSaleItemsUrl(getSaleItemStatusBasedOnViewType(), 
					CurrentUser.getCurrentUser().getLatitude(), CurrentUser.getCurrentUser().getLatitude(), sponsored);
		}
		saleItemApiTask = new ApiTaskFactory<SaleItemsResponse>().newInstance(getActivity(), 
				url, APIMethod.GET, SaleItemsResponse.class, new GetSaleItemResponseHandler());
		saleItemApiTask.executeRequest(null, shouldResultCacheResponse);
	}
	
	@Override
	protected void initialize() {
		super.initialize();
		initView();
		update();
	}
	
	private void initView() {
		setFloatingActionButtonIconResId(R.drawable.ic_plus_white_20);
		showFloatingActionButton();
		this.absListView.setOnItemClickListener(this);
		this.swipeRefreshLayout.setEnabled(true);
		setTitle();
	}
	
	protected void setTitle() {
		((ActionBarActivity) getActivity()).setTitle(saleItemViewType.getTitle(getActivity()));
	}
	
	@Override
	public void onRefresh() {
		super.onRefresh();
		// clear hashset also to view items with animation again
		hashSetForOneTimeAnimation.clear();
		hashSetForOneTimeAnimation.add(0);
		getSaleItemFromApi(false);
	}
	
	protected void update() {
		if (saleItemApiTask == null && getActivity() != null) {
			if (isSaleItemByMeOnly) {
				// update data according to filteration
				listItems.clear();
				for (SaleItem saleItem : allSaleItemsList) {
					if (filteredItems[0] && saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_OPEN) {
						listItems.add(saleItem);
					}
					if (filteredItems[1] && saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_PENDING) {
						listItems.add(saleItem);
					}
					if (filteredItems[2] && saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_REJECTED) {
						listItems.add(saleItem);
					}
					if (filteredItems[3] && saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_SOLD) {
						listItems.add(saleItem);
					}
				}
			}
			
			super.update();
		}
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		if (saleItemApiTask != null) {
			saleItemApiTask.cancel();
		}
	}

//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		final View rootView = inflater.inflate(R.layout.fragment_post, container, false);
//		this.gridViewItems = (GridView) rootView.findViewById(R.id.list_view_post);
//		this.gridViewItems.setOnItemClickListener(this);
//
//		final FloatingActionButton button = (FloatingActionButton) rootView.findViewById(R.id.setter);
//		button.setSize(FloatingActionButton.SIZE_NORMAL);
//		button.setColorNormalResId(R.color.pink);
//		button.setColorPressedResId(R.color.pink_pressed);
//		button.setIcon(R.drawable.ic_pencil_wht_24dp);
//		button.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				final Intent intent = new Intent(SaleItemsFragment.this.getActivity(), DetailActivity.class);
//				Bundle bundle = ActivityBundleFactory.getBundle(null, null, ViewType.CREATE_POST);
//				intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
//				SaleItemsFragment.this.startActivity(intent);
//			}
//		});
//
//		//		final FloatingActionButton actionC = new FloatingActionButton(getActivity().getBaseContext());
//		//		((FloatingActionsMenu) findViewById(R.id.multiple_actions)).addButton(actionC);
//		//
//		//		final FloatingActionButton actionA = (FloatingActionButton) findViewById(R.id.action_a);
//		//		actionA.setOnClickListener(new OnClickListener() {
//		//			@Override
//		//			public void onClick(View view) {
//		//				actionA.setTitle("Action A clicked");
//		//			}
//		//		});
//
//
//		this.initialize();
//		return rootView;
//	}

//	private void initialize() {
//		this.gridViewItems.setAdapter(this.postListAdapter);
//	}
	
	@Override
	public void onClick(View view) {
		if (view == floatingActionButton) {
			if (CurrentUser.getCurrentUser().getAccountStatus() == User.ACCOUNT_STATUS_PENDING) {
				MGSApplication.showToast("Unable to sale new item since User account is not activated.", Toast.LENGTH_LONG);
				return;
			}
			final Intent intent = new Intent(SaleItemsFragment.this.getActivity(), DetailActivity.class);
			Bundle bundle = new Bundle();
			bundle.putSerializable(ActivityBundleFactory.KEY_FRAGMENT_CLASS_NAME, AddNewSaleItemFragment.class);
			bundle.putBoolean(AddNewSaleItemFragment.KEY_CREATE_SPONSORED_ITEM, false);
			intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
			SaleItemsFragment.this.startActivity(intent);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		onSaleItemClick(adapterView, view, position, id);
	}

	@Override
	protected AbsListView getAbsListView() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		return (StaggeredGridView) inflater.inflate(R.layout.grid_view_sale_items, null);
	}

	@Override
	protected View getItemView(final int position, final SaleItem saleItem, View convertView, final ViewGroup parent) {
		View view = convertView;
		ViewHolder viewHolder = null;
		if (view == null) {
			final LayoutInflater layoutInflater = (LayoutInflater) SaleItemsFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.view_sale_items, parent, false);
			viewHolder = new ViewHolder();
			viewHolder.textViewSaleItemTitle = (TextView) view.findViewById(R.id.text_view_sale_item_title);
			viewHolder.textViewSaleItemPrice = (TextView) view.findViewById(R.id.text_view_sale_item_price);
			viewHolder.textViewSaleItemModifiedDate = (TextView) view.findViewById(R.id.text_view_sale_item_modified_date);
			viewHolder.viewSeparator = view.findViewById(R.id.view_separator);
			viewHolder.imageViewSaleItemOne = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_one);
			viewHolder.imageViewSaleItemTwo = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_two);
			viewHolder.imageViewSaleItemThree = (NetworkImageView) view.findViewById(R.id.image_view_sale_item_three);
			viewHolder.layout_sale_items_images = (ViewGroup) view.findViewById(R.id.layout_sale_items_images);
			viewHolder.imageButtonContactSeller = (ImageButton) view.findViewById(R.id.image_button_email_seller);
			viewHolder.layoutSideImages = (ViewGroup) view.findViewById(R.id.layout_side_images);
			view.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) view.getTag();
		}
		if (hashSetForOneTimeAnimation.add(position)) {
			view.setAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.slide_in_bottom));
		}
		
		if (saleItem.getSponsored() == SaleItem.SALE_ITEM_SPONSORED) {
			Drawable drawable = getResources().getDrawable(R.drawable.ic_sponsored);
			drawable.setBounds(0, 0, SizeUtil.getSizeInPixels(18), SizeUtil.getSizeInPixels(18));
			viewHolder.textViewSaleItemTitle.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
			viewHolder.textViewSaleItemTitle.setCompoundDrawablePadding(SizeUtil.getSizeInPixels(4));
		} else {
			viewHolder.textViewSaleItemTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
			viewHolder.textViewSaleItemTitle.setCompoundDrawablePadding(0);
		}
		viewHolder.textViewSaleItemTitle.setText(saleItem.getTitle());
		viewHolder.textViewSaleItemPrice.setText(String.format("$ %d", saleItem.getPrice()));
		
		String categories = getCategoriesText(saleItem);
		if (StringUtil.isNullOrEmpty(categories)) {
			viewHolder.textViewSaleItemModifiedDate.setVisibility(View.GONE);
		} else {
			viewHolder.textViewSaleItemModifiedDate.setVisibility(View.VISIBLE);
			viewHolder.textViewSaleItemModifiedDate.setText(categories);
		}
		
		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
		
		ArrayList<String> imageIds = saleItem.getImages();
		if (imageIds == null || imageIds.isEmpty()) {
			viewHolder.layout_sale_items_images.setVisibility(View.GONE);
			viewHolder.viewSeparator.setVisibility(View.GONE);
		} else {
			viewHolder.layout_sale_items_images.setVisibility(View.VISIBLE);
			viewHolder.viewSeparator.setVisibility(View.VISIBLE);
			int imagesCount = imageIds.size();
			viewHolder.imageViewSaleItemOne.setVisibility(View.VISIBLE);
			viewHolder.imageViewSaleItemOne.setImageUrl(UrlUtil.getImageUrl(imageIds.get(0)), 
					imageLoader);
			
			if (imagesCount > 1) {
				viewHolder.layoutSideImages.setVisibility(View.VISIBLE);
				viewHolder.imageViewSaleItemTwo.setVisibility(View.VISIBLE);
				viewHolder.imageViewSaleItemTwo.setImageUrl(UrlUtil.getImageUrl(imageIds.get(1)), 
						imageLoader);
			} else {
				viewHolder.layoutSideImages.setVisibility(View.GONE);
				viewHolder.imageViewSaleItemTwo.setVisibility(View.GONE);
				viewHolder.imageViewSaleItemThree.setVisibility(View.GONE);
			}
			if (imagesCount > 2) {
				viewHolder.layoutSideImages.setVisibility(View.VISIBLE);
				viewHolder.imageViewSaleItemThree.setVisibility(View.VISIBLE);
				viewHolder.imageViewSaleItemThree.setImageUrl(UrlUtil.getImageUrl(imageIds.get(2)), 
						imageLoader);
			} else {
				viewHolder.imageViewSaleItemThree.setVisibility(View.GONE);
			}
		} 		
		
		final View currentView = view;
		// enabling item click of grid view
		
//		// TODO find out better solution than this
		SaleItemViewClickListener listener = new SaleItemViewClickListener((AdapterView<?>) parent, currentView, position);
			
		view.setOnClickListener(listener);
		
		// hide mail icon if sale item is created by me
//		if (saleItem.getSeller().getUserId().equals(CurrentUser.getCurrentUser().getUserId())) {
//			viewHolder.imageButtonContactSeller.setVisibility(View.GONE);
//		} else {
//			// hiding/showing emmail id
//			if (StringUtil.isNullOrEmpty(saleItem.getSeller().getEmailId())) {
//				viewHolder.imageButtonContactSeller.setVisibility(View.GONE);
//			} else {
//				viewHolder.imageButtonContactSeller.setVisibility(View.VISIBLE);
//			}
//		}
		
		viewHolder.imageButtonContactSeller.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getActivity(), DetailActivity.class);
				Bundle bundle = ActivityBundleFactory.getBundle(saleItem.getSeller().getUserId(), saleItem.getSeller(), ViewType.MESSAGE_BETWEEN_USER);
				intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
				startActivity(intent);
			}
		});
		
		return view;
	}
	
	private class SaleItemViewClickListener implements OnClickListener {
		
		private AdapterView<?> parent;
		private View currentView;
		private int position;
				
		public SaleItemViewClickListener(AdapterView<?> parent,
				View currentView, int position) {
			super();
			this.parent = parent;
			this.currentView = currentView;
			this.position = position;
		}

		@Override
		public void onClick(View v) {
			SaleItemsFragment.this.onSaleItemClick((AdapterView<?>) parent, currentView, position + 1, currentView.getId());
		}
	}
	
	protected void onSaleItemClick(AdapterView<?> adapterView, View view, int position, long id) {
		SaleItem saleItem = listItems.get(position - 1);
		final Intent intent = new Intent(this.getActivity(), DetailActivity.class);
		Bundle bundle = ActivityBundleFactory.getBundle(saleItem.getItemId(), saleItem, ViewType.SALE_ITEM_DETAIL);
		intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
		this.startActivityForResult(intent, REQUEST_CODE_SALE_ITEM_DETAIL);
	}

	@Override
	protected void displayNoItemsMessage() {
		messageView.showMessage("No Items Found", R.drawable.ic_shopping_basket_grey600_48dp, false);
	}

	private static class ViewHolder {
		TextView textViewSaleItemTitle;
		TextView textViewSaleItemPrice;
		TextView textViewSaleItemModifiedDate;
		View viewSeparator;
		ImageButton imageButtonContactSeller;
		NetworkImageView imageViewSaleItemOne;
		NetworkImageView imageViewSaleItemTwo;
		NetworkImageView imageViewSaleItemThree;
		ViewGroup layout_sale_items_images;
		HorizontalScrollView scroll_view_sale_items_images;
		ViewGroup layoutSideImages;
	}

//	@Override
//	protected ArrayList<String> getListItems() {
//		ArrayList<String> list = new ArrayList<>();
//		return null;
//	}
	
	public enum SaleItemViewType {
		MY_OPEN_SALE_ITEM(0),
		MY_PENDING_SALE_ITEM(1),
		MY_SOLD_SALE_ITEM(2),
		MY_REJECTED_SALE_ITEM(3),
		MY_SALE_ITEM(9),
		ALL_POSTS(4),
		ALL_OPEN_SALE_ITEM(5),
		ALL_PENDING_SALE_ITEM(6),
		ALL_SOLD_SALE_ITEM(7),
		ALL_REJECTED_SALE_ITEM(8),
		SPONSORED_SALE_ITEM(10);
		
		private int value;
		
		private SaleItemViewType(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
		
		public static SaleItemViewType getSaleItemViewType(int value) {
			SaleItemViewType saleItemViewType = null;
			for (SaleItemViewType itemViewType : SaleItemViewType.values()) {
				if (value == itemViewType.value) {
					saleItemViewType = itemViewType;
					break;
				}
			}
			return saleItemViewType;
		}
		
		public String getTitle(Context context) {
			String title = context.getResources().getString(R.string.menu_item_all_posts);
			switch (this) {
			case MY_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_my_posts);
				break;
			case MY_OPEN_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_my_open_posts);
				break;
			case MY_PENDING_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_my_pending_posts);
				break;
			case MY_REJECTED_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_my_rejected_posts);
				break;
			case MY_SOLD_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_my_sold_posts);
				break;
			case ALL_OPEN_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_all_posts);
				break;
			case ALL_PENDING_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_all_posts);
				break;
			case ALL_REJECTED_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_all_posts);
				break;
			case ALL_SOLD_SALE_ITEM:
				title = context.getResources().getString(R.string.menu_item_all_posts);
				break;
			default:
				title = context.getResources().getString(R.string.menu_item_all_posts);
				break;
			}
			return title;
		}
	}
	
	private void filterItems() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Filter Items");
		CharSequence[] itemTitles = {"Open Items", "Pending Items", "Rejected Items", "Sold Items"};
		builder.setMultiChoiceItems(itemTitles, filteredItems, new DialogInterface.OnMultiChoiceClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which, boolean isChecked) {
				filteredItems[which] = isChecked;
			}
		});
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				update();
			}
		});
		builder.setNegativeButton("CANCEL", null);
		AlertDialog dialog = builder.create();
		dialog.show();
	}
	
	protected class GetSaleItemResponseHandler extends ApiResponseHandler<SaleItemsResponse> {
		
		@Override
		public void onApiSuccess(SaleItemsResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			saleItemApiTask = null;
			SaleItemsFragment.this.error = null;
			if (!isCachedResponse) {
				setLastUpdatedTime(System.currentTimeMillis());
				swipeRefreshLayout.setRefreshing(false);
				swipeRefreshLayout.setEnabled(true);
			} else {
				swipeRefreshLayout.setRefreshing(true);
			}
			
			if (isSaleItemByMeOnly) {
				allSaleItemsList.clear();
				if (response.getSaleItems() != null) {
					allSaleItemsList.addAll(response.getSaleItems());
				}
			} else {
				listItems.clear();
				if (response.getSaleItems() != null) {
					listItems.addAll(response.getSaleItems());
				}
			}
			
			if (getActivity() != null) {
				update();
			}
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			swipeRefreshLayout.setEnabled(true);
			saleItemApiTask = null;
			swipeRefreshLayout.setRefreshing(false);
			SaleItemsFragment.this.error = error;
			if (getActivity() != null) {
				update();
			}
		}
	}
	
//	private ArrayList<SaleItem> getSaleItemsBasedOnViewType(ArrayList<SaleItem> response) {
//		ArrayList<SaleItem> saleItems = new ArrayList<>();
//		for (SaleItem saleItem : response) {
//			switch (saleItemViewType) {
//				case ALL_POSTS:
//					saleItems.add(saleItem);
//					break;
//					
//				case ALL_OPEN_SALE_ITEM:
//				case MY_OPEN_SALE_ITEM:
//					if (saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_OPEN) {
//						saleItems.add(saleItem);
//					}
//					break;
//					
//				case ALL_PENDING_SALE_ITEM:
//				case MY_PENDING_SALE_ITEM:
//					if (saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_PENDING) {
//						saleItems.add(saleItem);
//					}
//					break;
//					
//				case ALL_SOLD_SALE_ITEM:
//				case MY_SOLD_SALE_ITEM:
//					if (saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_SOLD) {
//						saleItems.add(saleItem);
//					}
//					break;
//					
//				case ALL_REJECTED_SALE_ITEM:
//				case MY_REJECTED_SALE_ITEM:
//					if (saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_REJECTED) {
//						saleItems.add(saleItem);
//					}
//					break;
//				default:
//					break;
//			}
//		}
//		return saleItems;
//	}
	
	private int getSaleItemStatusBasedOnViewType() {
		int saleItemStatus = SaleItem.SALE_ITEM_STATUS_OPEN;
			switch (saleItemViewType) {
				case ALL_POSTS:
				case MY_SALE_ITEM:
					// there is no constant for this, also any other value then defined gives all items
					saleItemStatus = 0;
					break;
				case ALL_OPEN_SALE_ITEM:
				case MY_OPEN_SALE_ITEM:
					saleItemStatus = SaleItem.SALE_ITEM_STATUS_OPEN;
					break;
					
				case ALL_PENDING_SALE_ITEM:
				case MY_PENDING_SALE_ITEM:
					saleItemStatus = SaleItem.SALE_ITEM_STATUS_PENDING;
					break;
					
				case ALL_SOLD_SALE_ITEM:
				case MY_SOLD_SALE_ITEM:
					saleItemStatus = SaleItem.SALE_ITEM_STATUS_SOLD;
					break;
					
				case ALL_REJECTED_SALE_ITEM:
				case MY_REJECTED_SALE_ITEM:
					saleItemStatus = SaleItem.SALE_ITEM_STATUS_REJECTED;
					break;
				default:
					break;
			}
		
		return saleItemStatus;
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		showView(ContentViewType.CONTENT_LOADING);
		this.getSaleItemFromApi(true);
	}
	
	protected String getCategoriesText(SaleItem saleItem) {
		StringBuffer text = new StringBuffer();
		if (saleItem.getCategories() != null) {
			for (String categoryNmae : saleItem.getCategories()) {
				text.append(categoryNmae + ", ");
			}
		}
		// remove last ,
		try {
			text.delete(text.length() - 2, text.length() - 1);
		} catch(Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return text.toString().trim();
	}
}
