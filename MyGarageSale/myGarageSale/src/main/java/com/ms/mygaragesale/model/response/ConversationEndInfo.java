package com.ms.mygaragesale.model.response;

import java.io.Serializable;

public class ConversationEndInfo implements Serializable {

	private static final long serialVersionUID = -1496831985759478286L;
	
	private String id;
	private String name;
	private String imageId;
	
	public ConversationEndInfo() {
		// TODO Auto-generated constructor stub
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageId() {
		return imageId;
	}

	public void setImageId(String imageId) {
		this.imageId = imageId;
	}
	
	

}
