package com.ms.mygaragesale.ui;

import io.fabric.sdk.android.Fabric;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.ms.garagesaledeal.BuildConfig;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.Constants;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.MGSError.MGSErrorType;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.db.DatabaseProvider;
import com.ms.mygaragesale.core.util.AuthUtility;
import com.ms.mygaragesale.core.util.AuthUtility.AuthManagerStatusListener;
import com.ms.mygaragesale.core.util.AuthUtility.AuthStatus;
import com.ms.mygaragesale.core.util.LogoutUtility;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.response.AlertKeywordsResponse;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;

public class SplashActivity extends MGSActivity implements AuthManagerStatusListener {

	private static final String consumerKey = "72tfDPQQdx7YwaugrmTMvYE08";
	private static final String consumerSecret = "AEDOsUu290nhMKGuazUqi0gXgVovFXf7b8rrCoeJmHhJPyQjj5";

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

	private WaitAsyncTask waitAsyncTask;
	private long timeStamp = System.currentTimeMillis();
	
	private AuthUtility authManager;
	private TextView textViewAppName;
	private ViewGroup layoutAppName;
	private ProgressBar progressBar;

    private GoogleCloudMessaging googleCloudMessaging;

    private String gcmRegistrationId;

	@Override
	protected void onCreate(Bundle savedInstance) {
		super.onCreate(savedInstance);
		this.setupFabric();
        this.setupGCM();
		
		this.setContentView(R.layout.activity_splash);
		layoutAppName = (ViewGroup) findViewById(R.id.layout_app_name);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar_splash);
		textViewAppName = (TextView) findViewById(R.id.text_view_app_name_start);
		Typeface typeface = Typeface.createFromAsset(getAssets(), "futura_light.ttf");
		textViewAppName.setTypeface(typeface, Typeface.NORMAL);
		
		TextView textViewAppNameEnd = (TextView) findViewById(R.id.text_view_app_name_end);
		textViewAppNameEnd.setTypeface(typeface, Typeface.BOLD);
		
		authManager = AuthUtility.newInstance(this);
		authManager.processIncompleteAuthentication();
		
		loadAnimation();
		
		PackageInfo info = null;
		try {
		    info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
		    for (Signature signature : info.signatures) {
		        MessageDigest md;
		        md = MessageDigest.getInstance("SHA");
		        md.update(signature.toByteArray());
		        String something = new String(Base64.encode(md.digest(), 0));
		        //String something = new String(Base64.encodeBytes(md.digest()));
		        Log.e("hash key", something);
		    }
		} catch (NameNotFoundException e1) {
		    Log.e("name not found", e1.toString());
		} catch (NoSuchAlgorithmException e) {
		    Log.e("no such an algorithm", e.toString());
		} catch (Exception e) {
		    Log.e("exception", e.toString());
		}

        this.handleIntent(getIntent());
	}

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    private void handleIntent(Intent intent) {
        Logger.log(Log.DEBUG, "DEBUG::SplashActivity", "handleIntent");
        if (intent.getExtras() != null) {
            Bundle data = (Bundle) intent.getExtras().get("key");
            if (data != null) {
                String title = data.getString("BUNDLE_KEY_SALE_ITEM_TITLE");
                Logger.log(Log.DEBUG, "DEBUG::SplashActivity", data.toString());
            }
            // launch main activity with given intent
        } else {
            // do other stuffs
        }
    }

    private void setupFabric() {
		TwitterAuthConfig authConfig = new TwitterAuthConfig(consumerKey, consumerSecret);
        Crashlytics crashlytics = new Crashlytics.Builder().disabled(BuildConfig.DEBUG).build();
		Fabric.with(this, crashlytics, new Twitter(authConfig));
	}

    private void setupGCM() {
        if (checkPlayServices()) {
            gcmRegistrationId = getRegistrationId();

            if (gcmRegistrationId == null || gcmRegistrationId.isEmpty()) {
                registerInBackground();
            }
        } else {
            Logger.log(Log.INFO, "DEBUG::SplashActivity", "No valid Google Play Services APK found.");
        }
    }

    /**
     * Check the device to make sure it has the Google Play Services APK. If
     * it doesn't, display a dialog that allows users to download the APK from
     * the Google Play Store or enable it in the device's system settings.
     */
    private boolean checkPlayServices() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, this,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Logger.log(Log.INFO, "DEBUG::SplashActivity", "This device is not supported.");
            }
            return false;
        }
        return true;
    }

    /**
     * Stores the registration ID and the app versionCode in the application's
     * {@code SharedPreferences}.
     *
     * @param regId registration ID
     */
    private void storeRegistrationId(String regId) {
        AppSharedPref sharedPref = new AppSharedPref();
        sharedPref.setGCMRegistrationId(regId);
        sharedPref.setAppRegisteredVersion(getAppVersion());
    }

    /**
     * Gets the current registration ID for application on GCM service, if there is one.
     * <p>
     * If result is empty, the app needs to register.
     *
     * @return registration ID, or empty string if there is no existing
     *         registration ID.
     */
    private String getRegistrationId() {
        AppSharedPref sharedPref = new AppSharedPref();
        int registeredVersion = sharedPref.getAppRegisteredVersion();
        int currentVersion = getAppVersion();
        if (registeredVersion != currentVersion) {
            Logger.log(Log.DEBUG, "DEBUG::SplashActivity", "App version changed");
            return null;
        }

        String regId = sharedPref.getGCMRegistrationId();
        return regId;
    }

    /**
     * Registers the application with GCM servers asynchronously.
     * <p>
     * Stores the registration ID and the app versionCode in the application's
     * shared preferences.
     */
    private void registerInBackground() {
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                String msg = "";
                try {
                    if (googleCloudMessaging == null) {
                        googleCloudMessaging = GoogleCloudMessaging.getInstance(MGSApplication.getInstance().getApplicationContext());
                    }
                    gcmRegistrationId = googleCloudMessaging.register(Constants.GCM_SENDER_ID);
                    msg = "Device registered, registration ID=" + gcmRegistrationId;

                    // You should send the registration ID to your server over HTTP, so it
                    // can use GCM/HTTP or CCS to send messages to your app.
                    sendRegistrationIdToBackend();

                    // For this demo: we don't need to send it because the device will send
                    // upstream messages to a server that echo back the message using the
                    // 'from' address in the message.

                    // Persist the regID - no need to register again.
                    storeRegistrationId(gcmRegistrationId);
                } catch (IOException ex) {
                    msg = "Error :" + ex.getMessage();
                    // If there is an error, don't just keep trying to register.
                    // Require the user to click a button again, or perform
                    // exponential back-off.
                }
                return msg;
            }

            @Override
            protected void onPostExecute(String msg) {
                Logger.log(Log.DEBUG, "DEBUG::SplashActivity", msg);
            }
        }.execute(null, null, null);
    }

    /**
     * @return Application's version code from the {@code PackageManager}.
     */
    private static int getAppVersion() {
        try {
            Context context = MGSApplication.getInstance().getApplicationContext();
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    /**
     * Sends the registration ID to your server over HTTP, so it can use GCM/HTTP or CCS to send
     * messages to your app. Not needed for this demo since the device sends upstream messages
     * to a server that echoes back the message using the 'from' address in the message.
     */
    private void sendRegistrationIdToBackend() {
        // TODO if user is already logged in update registration id with this current accessToken, else it will be send to server during login process
    }

	private void loadAnimation() {
		
		layoutAppName.animate()
					.alpha(1)
					.setDuration(150)
					.setInterpolator(new AccelerateInterpolator())
					.start();
		
		layoutAppName.animate()
					.translationY(getTranslatedByValue())
					.setDuration(getNameTranslationDuration())
					.setInterpolator(new AccelerateDecelerateInterpolator())
					.setStartDelay(350)
					.start();
		
		progressBar.animate()
					.alpha(1)
					.setDuration(300)
					.setInterpolator(new AccelerateDecelerateInterpolator())
					.setStartDelay(400)
					.start();
		
		progressBar.animate()
					.translationY(SizeUtil.getSizeInPixels(30))
					.setDuration(getProgressTranslationDuration())
					.setInterpolator(new AccelerateDecelerateInterpolator())
					.setStartDelay(400)
					.start();
	}
	
	private int getTranslatedByValue() {
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_PORTRAIT) {
			return SizeUtil.getSizeInPixels(130);
		} else {
			int avPix = getResources().getDisplayMetrics().widthPixels;
			avPix /= 2;
			avPix = avPix - SizeUtil.getSizeInPixels(20);
			return Math.min(avPix, SizeUtil.getSizeInPixels(130));
		}
	}
	
	private int getNameTranslationDuration() {
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_PORTRAIT) {
			return 600;
		} else {
			return 500;
		}
	}
	
	private int getProgressTranslationDuration() {
		int orientation = getResources().getConfiguration().orientation;
		if (orientation == Configuration.ORIENTATION_PORTRAIT) {
			return 400;
		} else {
			return 300;
		}
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		layoutAppName.animate().translationY(getTranslatedByValue()).setDuration(0).start();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (authManager != null) {
			//authManager.cancel();
		}
		if (this.waitAsyncTask != null) {
			this.waitAsyncTask.cancel(true);
		}
	}

//	@Override
//	protected void onResume() {
//		super.onResume();
//		// Logs 'install' and 'app activate' App Events.
//		AppEventsLogger.activateApp(this);
//	}
//
//	@Override
//	protected void onPause() {
//		super.onPause();
//		// Logs 'app deactivate' App Event.
//		AppEventsLogger.deactivateApp(this);
//	}

	private void invokeActivity() {
		Class<? extends ActionBarActivity> activityToLaunchClass = null;
		AuthStatus authStatus = authManager.getAuthStatus();
		if (authStatus == AuthStatus.LOGGED_IN) {
			activityToLaunchClass = MainActivity.class;
		} else {
			activityToLaunchClass = AuthenticationActivity.class;
		}
		Intent intent = new Intent(SplashActivity.this, activityToLaunchClass);
		startActivity(intent);
		this.finish();
	}
	
	
	private class WaitAsyncTask extends AsyncTask<Void, Void, Void> {

		public WaitAsyncTask(Context context) {
			super();
		}

		@Override
		protected Void doInBackground(Void... params) {
			try {
				long sleepTime = System.currentTimeMillis() - timeStamp;
				sleepTime = Math.min(Math.max(0, 2000 - sleepTime), 2000);
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (!isCancelled()) {
				invokeActivity();
			}
		}
	}

	@Override
	public void onSuccess() {
		Logger.log("Login Success");
		CurrentUser currentUser = CurrentUser.getCurrentUser();
		if (currentUser.isDataSyncd() == false) {
			// update user changes to server
			ApiTask<CurrentUser> apiTask = new ApiTaskFactory<CurrentUser>().newInstance(getApplicationContext(), 
					UrlUtil.getUserUrl(String.valueOf(CurrentUser.getCurrentUser().getUserId())), 
						APIMethod.POST, CurrentUser.class, new UserResponseHandler());
			apiTask.executeRequest(currentUser, false);
		} else {
			// load user data and store to server
			ApiTask<CurrentUser> apiTask = new ApiTaskFactory<CurrentUser>().newInstance(getApplicationContext(), 
					UrlUtil.getUserUrl(String.valueOf(CurrentUser.getCurrentUser().getUserId())), 
						APIMethod.GET, CurrentUser.class, new UserResponseHandler());
			apiTask.executeRequest(null, false);
		}
		// refresh alert keywords from server
		this.getAlertKeywordsFromApi();
		
		this.waitAsyncTask = new WaitAsyncTask(this);
		this.waitAsyncTask.execute();
	}

	@Override
	public void onFailure(MGSError error) {
		Logger.log(error.toString());
		if (error.getErrorType() != MGSErrorType.NO_USER_LOGGED_IN 
				&& error.getErrorType() != MGSErrorType.FB_LOGIN_ERROR 
					&& error.getErrorType() != MGSErrorType.TWITTER_LOGIN_ERROR) {
			// TODO display dialog error
//			showErrorDialog(error);
//			MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
			
			// in case of any error logout user and prompt him to login again
			LogoutUtility.processLogoutInstantly(this);
		} else {
		
			this.waitAsyncTask = new WaitAsyncTask(this);
			this.waitAsyncTask.execute();
		}
		
	}

	@Override
	public void onMessage(String message) {
		Logger.log(message);
	}
	
	private void showErrorDialog(MGSError error) {
		// two buttons cancel and retry
	}
	
	private class UserResponseHandler extends ApiResponseHandler<CurrentUser> {
				
		@Override
		public void onApiSuccess(CurrentUser response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			if (response != null) {
				CurrentUser user = CurrentUser.getCurrentUser();
				user.copyNotNullValueWithUser(response);
				user.updateCurrentUser();
			}
		}
	}
	
	private void getAlertKeywordsFromApi() {
		String url = null;
		url = UrlUtil.getAlertKeywordsUrl();
		ApiTask<AlertKeywordsResponse> apiTask = new ApiTaskFactory<AlertKeywordsResponse>().newInstance(this,
				url, APIMethod.GET, AlertKeywordsResponse.class, new GetCategoriesResponseHandler());
		apiTask.executeRequest(null, false);
	}
	
	private class GetCategoriesResponseHandler extends ApiResponseHandler<AlertKeywordsResponse> {

		@Override
		public void onApiSuccess(AlertKeywordsResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			if (!isCachedResponse) {
				new AppSharedPref().setAlertKeywordsLastUpdatedTime(System.currentTimeMillis());
				if (response.getAlertKeywords() != null) {
					// save data to database
					DatabaseProvider.getInstance().updateAlertKeywords(response.getAlertKeywords());
				}
			}
		}
	}
}
