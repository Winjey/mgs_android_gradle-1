package com.ms.mygaragesale.model.request;

import java.util.ArrayList;

import com.ms.mygaragesale.model.Location;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.model.response.Response;

public class SaleItemRequest extends Response {
	
	private String itemId;
	private String title;
	private String description;
	private float price;
	private String sellerId;
	private ArrayList<String> images;
	private ArrayList<String> categories;
	private Location location;
	private int sponsored;
	
	public SaleItemRequest() {
		
	}

//	public SaleItemRequest(String itemId, String title, String description,
//			long price, Date createdDate, Date modifiedDate, int itemStatus,
//			String sellerId, String messageId, int messageCount, String[] images) {
//		super();
//		this.itemId = itemId;
//		this.title = title;
//		this.description = description;
//		this.price = price;
//		this.createdDate = createdDate;
//		this.modifiedDate = modifiedDate;
//		this.itemStatus = itemStatus;
//		this.sellerId = sellerId;
//		this.messageId = messageId;
//		this.messageCount = messageCount;
//		this.images = images;
//	}
	
	public SaleItemRequest(SaleItem saleItem) {
		super();
		this.itemId = saleItem.getItemId();
		this.title = saleItem.getTitle();
		this.description = saleItem.getDescription();
		this.price = saleItem.getPrice();
		this.sellerId = saleItem.getSeller().getUserId();
		this.images = saleItem.getImages();
		this.categories = saleItem.getCategories();
		this.location = saleItem.getLocation();
        this.sponsored = saleItem.getSponsored();
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(long price) {
		this.price = price;
	}

//	public Date getCreatedDate() {
//		return createdDate;
//	}
//
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//
//	public Date getModifiedDate() {
//		return modifiedDate;
//	}
//
//	public void setModifiedDate(Date modifiedDate) {
//		this.modifiedDate = modifiedDate;
//	}

//	public int getItemStatus() {
//		return itemStatus;
//	}
//
//	public void setItemStatus(int itemStatus) {
//		this.itemStatus = itemStatus;
//	}

	public String getSellerId() {
		return sellerId;
	}

	public void setSellerId(String sellerId) {
		this.sellerId = sellerId;
	}

//	public String getMessageId() {
//		return messageId;
//	}
//
//	public void setMessageId(String messageId) {
//		this.messageId = messageId;
//	}
//
//	public int getMessageCount() {
//		return messageCount;
//	}
//
//	public void setMessageCount(int messageCount) {
//		this.messageCount = messageCount;
//	}

	public ArrayList<String> getImages() {
		return images;
	}
	
	public void setImages(ArrayList<String> images) {
		this.images = images;
	}
	
	public ArrayList<String> getCategories() {
		return categories;
	}
	
	public void setCategories(ArrayList<String> categories) {
		this.categories = categories;
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public int getSponsored() {
		return sponsored;
	}
	
	public void setSponsored(int sponsored) {
		this.sponsored = sponsored;
	}
}
