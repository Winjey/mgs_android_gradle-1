package com.ms.mygaragesale.model;

import java.util.ArrayList;
import java.util.Date;

import com.ms.mygaragesale.model.response.Response;

public class SaleItem extends Response implements BaseItem {

	private static final long serialVersionUID = 2639246091366941207L;
	
	public static final int SALE_ITEM_STATUS_OPEN = 1;
	public static final int SALE_ITEM_STATUS_SOLD = 2;
	public static final int SALE_ITEM_STATUS_PENDING = 3;
	public static final int SALE_ITEM_STATUS_REJECTED = 4;
	
	public static final int SALE_ITEM_SPONSORED = 1;
	public static final int SALE_ITEM_NOT_SPONSORED = 0;
	
	private String itemId;
	private String title;
	private String description;
	private int price;
	private Date createdDate;
	private Date modifiedDate;
	private int itemStatus;
	private User seller;
    private ArrayList<Message> messages;
	private ArrayList<String> images;
	private ArrayList<String> categories;
	private Location location;
	private int sponsored;

	public SaleItem() {
	}

	public SaleItem(String itemId, String title, String description,
			int price, Date createdDate, Date modifiedDate, int itemStatus,
			User seller, ArrayList<String> images,
			ArrayList<String> categories, Location location, int sponsored) {
		super();
		this.itemId = itemId;
		this.title = title;
		this.description = description;
		this.price = price;
		this.createdDate = createdDate;
		this.modifiedDate = modifiedDate;
		this.itemStatus = itemStatus;
		this.seller = seller;
		this.images = images;
		this.categories = categories;
		this.location = location;
		this.sponsored = sponsored;
	}



	// public SaleItem(long id, String itemId, String title, String description,
	// long price, Date createdDate, Date modifiedDate, int itemStatus,
	// User seller, String messageId, int messageCount,
	// List<SaleItemImage> listImages, Long userId) {
	// super();
	// this.id = id;
	// this.itemId = itemId;
	// this.title = title;
	// this.description = description;
	// this.price = price;
	// this.createdDate = createdDate;
	// this.modifiedDate = modifiedDate;
	// this.itemStatus = itemStatus;
	// this.seller = seller;
	// this.messageId = messageId;
	// this.messageCount = messageCount;
	// this.listImages = listImages;
	// this.userId = userId;
	// }

	// public SaleItem(com.ms.mygaragesale.core.db.dao.SaleItemTable dbSaleItem)
	// {
	// this.id = dbSaleItem.getId();
	// this.itemId = dbSaleItem.getItemId();
	// this.title = dbSaleItem.getTitle();
	// this.description = dbSaleItem.getDescription();
	// this.price = dbSaleItem.getPrice();
	// this.createdDate = dbSaleItem.getCreatedDate();
	// this.modifiedDate = dbSaleItem.getModifiedDate();
	// List<SaleItemImage> images = new ArrayList<SaleItemImage>();
	// for (com.ms.mygaragesale.core.db.dao.SaleItemImageTable dbSaleItemImage :
	// dbSaleItem.getSaleItemImageTableList()) {
	// images.add(new SaleItemImage(dbSaleItemImage));
	// }
	// this.listImages = images;
	// this.itemStatus = dbSaleItem.getItemStatus();
	// this.seller = new User(dbSaleItem.getUserTable());
	// this.messageId = dbSaleItem.getMessageId();
	// this.messageCount = dbSaleItem.getMessageCount();
	// }
	//
	// public com.ms.mygaragesale.core.db.dao.SaleItemTable getDbSaleItem() {
	// SaleItemTable dbSaleItem = new SaleItemTable(id, itemId, title,
	// description, price, createdDate,
	// modifiedDate, messageId, itemStatus, messageCount, userId);
	// dbSaleItem.setUserTable(this.getSeller().getDbUser());
	// return dbSaleItem;
	// }

	// public boolean isDataSyncd() {
	// return isDataSyncd;
	// }
	//
	// public void setDataSyncd(boolean isDataSyncd) {
	// this.isDataSyncd = isDataSyncd;
	// }
	//
	// public long getId() {
	// return id;
	// }
	//
	// public void setId(long id) {
	// this.id = id;
	// }

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getModifeidDate() {
		return modifiedDate;
	}

	public void setModifeidDate(Date modifeidDate) {
		this.modifiedDate = modifeidDate;
	}

	// this method does not update db
	// public void setImagesList(List<SaleItemImage> listImages) {
	// this.listImages = listImages;
	// }
	//
	// public List<SaleItemImage> getImagesList() {
	// return listImages;
	// }

	public int getItemStatus() {
		return itemStatus;
	}

	public void setItemStatus(int itemStatus) {
		this.itemStatus = itemStatus;
	}

	public User getSeller() {
		return seller;
	}

	public void setSeller(User seller) {
		this.seller = seller;
	}

	// public String getMessagesId() {
	// return messageId;
	// }
	//
	// public void setMessagesId(String messagesId) {
	// this.messageId = messagesId;
	// }
	//
	// public int getMessageCount() {
	// return messageCount;
	// }
	//
	// public void setMessageCount(int messageCount) {
	// this.messageCount = messageCount;
	// }
	
//	public int getValidity() {
//		return validity;
//	}
//	
//	public void setValidity(int validity) {
//		this.validity = validity;
//	}

	public ArrayList<String> getImages() {
		return images;
	}

	public void setImages(ArrayList<String> images) {
		this.images = images;
	}

	public ArrayList<String> getCategories() {
		return categories;
	}

	public void setCategories(ArrayList<String> categories) {
		this.categories = categories;
	}
	
	public void setCategories(String categoriesStr) {
		this.categories = new ArrayList<String>();
		String[] commaSep = categoriesStr.split(",");
		if (commaSep != null) {
			for (int i = 0; i < commaSep.length; i++) {
				String catName = commaSep[i].trim();
				if (catName.length() > 0) {
					this.categories.add(catName);
				}
			}
		}
	}
	
	public Location getLocation() {
		return location;
	}
	
	public void setLocation(Location location) {
		this.location = location;
	}
	
	public boolean isSaledByMe() {
		return this.getSeller().getUserId().equals(CurrentUser.getCurrentUser().getUserId());
	}
	
	public int getSponsored() {
		return sponsored;
	}
	
	public void setSponsored(int sponsored) {
		this.sponsored = sponsored;
	}

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }
}
