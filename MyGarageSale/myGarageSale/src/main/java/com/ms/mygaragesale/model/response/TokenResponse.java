package com.ms.mygaragesale.model.response;

public class TokenResponse extends Response {
	
	private String accessToken;
	
	public TokenResponse(int errorCode, String errorMessage, String token) {
		super(errorCode, errorMessage);
		this.accessToken = token;
	}

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	

	
	
}
