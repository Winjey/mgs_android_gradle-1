package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.MGSError.MGSErrorType;

public class MessageView extends LinearLayout implements View.OnClickListener {

	private ImageView imageViewMessageIcon;
	private TextView textViewMessageTitle;
	private TextView textViewTapToRetry;
	
	private OnMessageViewTapToRetryListener onMessageViewTapToRetryListener;
	
	private boolean enableRetry;
	
	public MessageView(Context context) {
		super(context);
		init(null);
	}

	public MessageView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public MessageView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}
	
	public void setOnMessageViewTapToRetryListener(
			OnMessageViewTapToRetryListener onMessageViewTapToRetryListener) {
		this.onMessageViewTapToRetryListener = onMessageViewTapToRetryListener;
	}
	
	public void hideTapToRetry() {
		this.textViewTapToRetry.setVisibility(View.GONE);
	}
	
	/**
	 * Method to display default messages on basis of given input {@link MessageType} value.
	 * If null input is given then {@link MessageType}.{@link Error} message is displayed.  
	 * @param messageType
	 */
	public void showMessage(MGSError error) {		
		showMessage(error, true);
	}
	
	public void showMessage(MGSError error, boolean enableTapToRetry) {
		if (error == null) {
			error = new MGSError();
		}
		int iconRes = this.getErrorIconRes(error.getErrorType());		
		showMessage(error.getMesssage(), iconRes, enableTapToRetry);
	}
	
	
	/**
	 * Method to display given title, description, icon.
	 * @param title
	 * @param description
	 * @param iconResourceId
	 */
	public void showMessage(CharSequence message, int iconResourceId, boolean enableTapToRetry) {
		textViewMessageTitle.setText(message);
		imageViewMessageIcon.setImageResource(iconResourceId);
		int drawable = R.drawable.new_selector_transparent;
		if (enableTapToRetry) {
			textViewTapToRetry.setVisibility(View.VISIBLE);
			setOnClickListener(this);
			drawable = R.drawable.new_selector_button_flat;
		} else {
			textViewTapToRetry.setVisibility(View.GONE);
			setOnClickListener(null);
			drawable = R.drawable.new_selector_transparent;
		}
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
			setBackground(getResources().getDrawable(drawable));
		} else {
			setBackgroundDrawable(getResources().getDrawable(drawable));
		}
		enableRetry = enableTapToRetry;
	}
	
	private void init(AttributeSet attributeSet) {
		inflate(getContext(), R.layout.view_message, this);
		setOnClickListener(this);
		this.imageViewMessageIcon = (ImageView) findViewById(R.id.imageViewMessageIcon);
		this.textViewMessageTitle = (TextView) findViewById(R.id.textViewMessageTitle);
		this.textViewTapToRetry = (TextView) findViewById(R.id.text_view_tap_to_retry);
		
//		if (attributeSet != null) {
//			TypedArray typedArray = getContext().obtainStyledAttributes(attributeSet, R.styleable.MessageView);
//			
//			LayoutParams iconParams = (LayoutParams) this.imageViewMessageIcon.getLayoutParams();
//			iconParams.width = typedArray.getDimensionPixelSize(R.styleable.MessageView_messageIconWidth, iconParams.width);
//			iconParams.height = typedArray.getDimensionPixelSize(R.styleable.MessageView_messageIconHeight, iconParams.height);
//			this.imageViewMessageIcon.requestLayout();
//			
//			Drawable drawable = typedArray.getDrawable(R.styleable.MessageView_messageIconSrc);
//			this.imageViewMessageIcon.setImageDrawable(drawable);
//			
//			String title = typedArray.getString(R.styleable.MessageView_messageTitleText);
//			this.textViewMessageTitle.setText(title);
//			String desc = typedArray.getString(R.styleable.MessageView_messageDescriptionText);
//			this.textViewMessageDesc.setText(desc);
//			
//			int titleSizeInPixels = typedArray.getDimensionPixelSize(R.styleable.MessageView_messageTitleTextSize, (int) this.textViewMessageTitle.getTextSize());
//			int titleSize = (int) (titleSizeInPixels / getResources().getDisplayMetrics().scaledDensity);
//			this.textViewMessageTitle.setTextSize(titleSize);
//			int descSizeInPixels = typedArray.getDimensionPixelSize(R.styleable.MessageView_messageDescriptionTextSize, (int) this.textViewMessageDesc.getTextSize());
//			int descSize = (int) (descSizeInPixels / getResources().getDisplayMetrics().scaledDensity);
//			this.textViewMessageDesc.setTextSize(descSize);
//			
//			typedArray.recycle();
//		}
	}	
	
	public interface OnMessageViewTapToRetryListener {
		void onTapToRetry();
	}

	@Override
	public void onClick(View view) {
		if (onMessageViewTapToRetryListener != null && enableRetry) {
			onMessageViewTapToRetryListener.onTapToRetry();
		}
	}
	
	private int getErrorIconRes(MGSErrorType errorType) {
		int errorIcon = R.drawable.ic_error;
		switch (errorType) {
			case NO_NETWORK_CONNECTION:
				errorIcon = R.drawable.ic_frown_cloud;
				break;
				
			case API_ERROR:			
			case ERROR:
			default:
				errorIcon = R.drawable.ic_error;
				break;
		}
		return errorIcon;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		super.onTouchEvent(event);
		if (enableRetry) {
			return true;
		}
		return false;
	}
}
