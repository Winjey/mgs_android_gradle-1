package com.ms.mygaragesale.model;

import java.io.Serializable;

import com.ms.mygaragesale.core.db.dao.UserImageTable;

public class UserImage implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long id;
    private String imageId;
    private String imageUri;

    public UserImage() {
    }

    public UserImage(Long id) {
        this.id = id;
    }

    public UserImage(Long id, String imageId, String imageUri) {
        this.id = id;
        this.imageId = imageId;
        this.imageUri = imageUri;
    }
    
    public UserImage(UserImageTable dbUserImageTable) {
    	if (dbUserImageTable != null) {
    		this.id = dbUserImageTable.getId();
            this.imageId = dbUserImageTable.getImageId();
            this.imageUri = dbUserImageTable.getImageUri();
		}
    }
    
    public UserImageTable getDbUserImageTable() {
    	UserImageTable userImageTable = new UserImageTable(id, imageId, imageUri);
    	return userImageTable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

}
