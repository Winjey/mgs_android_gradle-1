package com.ms.mygaragesale.ui.frag;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.db.DatabaseProvider;
import com.ms.mygaragesale.core.util.DateUtil;
import com.ms.mygaragesale.core.util.ImageUtil;
import com.ms.mygaragesale.core.util.NotificationUtil;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.AlertKeyword;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.Message;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.request.AlertKeywordsRequest;
import com.ms.mygaragesale.model.request.MessageRequest;
import com.ms.mygaragesale.model.response.SuccessResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;
import com.ms.mygaragesale.ui.FullScreenActivity;
import com.ms.mygaragesale.ui.IActivityEventCallback;
import com.ms.mygaragesale.ui.view.MessagePosterView;
import com.ms.mygaragesale.ui.view.MessagePosterView.OnActionPostListener;

public class SaleItemDetailsFragment extends RefreshableItemsFragment<Message> 
			implements OnClickListener, IActivityEventCallback, OnActionPostListener {
	
	private ViewGroup layoutSaleItemsImagesContainer;
	private ViewGroup layoutImagesList;
	private ViewGroup layoutSeller;
	private ImageView imageViewSellerIcon;
	private TextView textViewSaleItemTitle;
	private TextView textViewSaleItemPrice;
	private TextView textViewSaleItemStatus;
	private TextView textViewSaleItemCreatedDate;
	private TextView textViewSaleItemDescription;
	private TextView textViewSaleItemCategories;
	private TextView textViewSellerName;
//	private TextView textViewAddress;
	private TextView textViewSaleItemComments;
	private TextView textViewContactSeller;
	private View viewSaleItemCommentSeparator;
	private ImageButton buttonAddComment;
	
	private ApiTask<SaleItem> messageApiTask;
	private ApiTask<Message> sendMessageApiTask;
	
	private SaleItem saleItem;
	private boolean isSaleItemChanged;
	
	private MessagePosterView messagePosterView;
	private ArrayList<AlertKeyword> alertKeywordsList;
	private boolean isKeywordAlreadyAddedForAlert;
	
	
	public static SaleItemDetailsFragment newInstance(SaleItem saleItem) {
		SaleItemDetailsFragment fragment = new SaleItemDetailsFragment();
		Bundle bundle = new Bundle();
		bundle.putSerializable(ActivityBundleFactory.KEY_SALE_ITEM_OBJECT, saleItem);
		fragment.setArguments(bundle);
		return fragment;
	}
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	Bundle bundle = getArguments();
    	saleItem = (SaleItem) bundle.getSerializable(ActivityBundleFactory.KEY_SALE_ITEM_OBJECT);

		// remove notification from status bar when opened it
		int notificationId = bundle.getInt(NotificationUtil.KEY_NOTIFICATION_ID);
		if (notificationId > 0) {
			NotificationUtil.resetItemAlertsNotification(notificationId);
		}

        if (saleItem.getMessages() != null) {
            listItems.addAll(saleItem.getMessages());
        }

    	getSaleItemFromApi(false);
		isKeywordAlreadyAddedForAlert = true;
		alertKeywordsList = DatabaseProvider.getInstance().getAlertKeywords();
    }
    
    @Override
	protected void initialize() {
    	super.initialize();
		initView();
		update();
	}
    
    private void initView() {
    	// add header
    	this.layoutSaleItemsImagesContainer = (ViewGroup) this.headerView.findViewById(R.id.layout_sale_items_images);
		this.layoutImagesList = (ViewGroup) this.headerView.findViewById(R.id.layout_images_list);
		this.imageViewSellerIcon = (ImageView) this.headerView.findViewById(R.id.image_view_creator_icon);
		this.textViewSaleItemTitle = (TextView) this.headerView.findViewById(R.id.text_view_post_title);
		this.textViewSaleItemPrice = (TextView) this.headerView.findViewById(R.id.text_view_post_price);
		this.textViewSaleItemStatus = (TextView) this.headerView.findViewById(R.id.text_view_post_status);
		this.textViewSaleItemCreatedDate = (TextView) this.headerView.findViewById(R.id.text_view_post_created_date);
		this.textViewSaleItemDescription = (TextView) this.headerView.findViewById(R.id.text_view_post_description);
		this.textViewSaleItemCategories = (TextView) this.headerView.findViewById(R.id.text_view_post_categories);
		this.textViewSellerName = (TextView) this.headerView.findViewById(R.id.text_view_creator_name);
//		this.textViewAddress = (TextView) this.headerView.findViewById(R.id.text_view_post_location);
		this.textViewSaleItemComments= (TextView) this.headerView.findViewById(R.id.text_view_post_comments);
		this.layoutSeller = (ViewGroup) this.headerView.findViewById(R.id.layout_seller);
		this.textViewContactSeller = (TextView) this.headerView.findViewById(R.id.text_view_seller_email);
		this.buttonAddComment = (ImageButton) this.headerView.findViewById(R.id.image_button_add_comment);
		this.viewSaleItemCommentSeparator = (View) this.headerView.findViewById(R.id.view_sale_item_comment_separator);
			
		// add footer
		messagePosterView = new MessagePosterView(getActivity());
		messagePosterView.setLayoutParams(new AbsListView.LayoutParams(AbsListView.LayoutParams.MATCH_PARENT, 
				SizeUtil.getSizeInPixels(70)));
		messagePosterView.setHint("Add Comment");
		messagePosterView.setOnActionPostListener(this);
		((ListView) absListView).addFooterView(messagePosterView);

		this.buttonAddComment.setOnClickListener(this);
		this.swipeRefreshLayout.setEnabled(true);
		this.hideFloatingActionButton();
        this.showView(ContentViewType.CONTENT_LOADING);
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (messageApiTask != null) {
			messageApiTask.cancel();
		} 
    }
    
    @Override
    protected void update() {
    	if (messageApiTask == null && getActivity() != null) {
			this.getActivity().invalidateOptionsMenu();
    		super.update();
    		((ActionBarActivity) getActivity()).setTitle(saleItem.getTitle());

			// check current status if item's seller is me, show message and return
			if (!saleItem.isSaledByMe()) {
				if (saleItem.getItemStatus() != SaleItem.SALE_ITEM_STATUS_OPEN) {
					showView(ContentViewType.CONTENT_MESSAGE);
					messageView.showMessage(getItemStatusNotOpenMessage(), R.drawable.ic_frown_cloud, false);
					// hide list view since we do not want this view to refresh also
					absListView.setVisibility(View.GONE);
					swipeRefreshLayout.setEnabled(false);
					return;
				}
			}

    		showView(ContentViewType.CONTENT_DATA);
    		this.loadAllImages();
    		textViewSaleItemTitle.setText(saleItem.getTitle());
    		if (saleItem.getSponsored() == SaleItem.SALE_ITEM_SPONSORED) {
    			textViewSaleItemTitle.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_sponsored_detail, 0, 0, 0);
    			textViewSaleItemTitle.setCompoundDrawablePadding(SizeUtil.getSizeInPixels(4));
			} else {
				textViewSaleItemTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
				textViewSaleItemTitle.setCompoundDrawablePadding(0);
			}
    		
    		textViewSaleItemPrice.setText(String.format("$ %d", saleItem.getPrice()));
    		textViewSaleItemCreatedDate.setText(DateUtil.getDateTimeText(saleItem.getCreatedDate()));
			// appending location to created date as per clients req
			if (saleItem.getLocation() != null && !StringUtil.isNullOrEmpty(saleItem.getLocation().getName())) {
				textViewSaleItemCreatedDate.append(" - " + saleItem.getLocation().getName());
			}

    		textViewSaleItemDescription.setText(saleItem.getDescription());
    		String categoriesText = getCategoriesText();
    		textViewSaleItemCategories.setText(categoriesText);
    		if (StringUtil.isNullOrEmpty(categoriesText)) {
    			textViewSaleItemCategories.setVisibility(View.GONE);
    		} 
    		
    		if (saleItem.isSaledByMe()) {
    			textViewSellerName.setText("Me");
    		} else {
    			textViewSellerName.setText(saleItem.getSeller().getName());
    		}

    		textViewSaleItemComments.setText("Comments(" + (listItems != null ? listItems.size() : 0) + ")");
    		loadUserIcon();
    		
    		if (listItems == null || listItems.isEmpty()) {
    			buttonAddComment.setVisibility(View.GONE);
			} else {
				buttonAddComment.setVisibility(View.VISIBLE);
			}
    		
    		if (saleItem.isSaledByMe()) {
				textViewContactSeller.setVisibility(View.GONE);
			} else {
				textViewContactSeller.setVisibility(View.VISIBLE);
				this.layoutSeller.setOnClickListener(this);
			}

    		//update account status if item created by me
    		setSaleItemStatusText();
			
    	}
    }
    
    private void setSaleItemStatusText() {
    	if (saleItem.getSeller().getUserId().equals(CurrentUser.getCurrentUser().getUserId()) 
    			|| saleItem.getSponsored() == SaleItem.SALE_ITEM_SPONSORED) {
			int color = 0;
			String text = null;
			int itemStatus = saleItem.getItemStatus();
			if (itemStatus == SaleItem.SALE_ITEM_STATUS_OPEN) {
				text =  "Item Approved";
				color = getResources().getColor(R.color.green_material_color);
			} else if (itemStatus == SaleItem.SALE_ITEM_STATUS_REJECTED) {
				text =  "Item Rejected";
				color = getResources().getColor(R.color.red_material_color);
			} else if (itemStatus == SaleItem.SALE_ITEM_STATUS_PENDING) {
				text =  "Item Approval Pending";
				color = getResources().getColor(R.color.yellow_material_color);
			} else if (itemStatus == SaleItem.SALE_ITEM_STATUS_SOLD) {
				text =  "Item Sold Out";
				color = getResources().getColor(R.color.blue_material_color);
			} 
			this.textViewSaleItemStatus.setVisibility(View.VISIBLE);
			this.textViewSaleItemStatus.setText(text);
			this.textViewSaleItemStatus.setTextColor(color);
    	} else {
			this.textViewSaleItemStatus.setVisibility(View.GONE);
		}
	}
    
    @Override
    protected long getLastUpdatedTime() {
    	return 0;
    }
    
    @Override
    protected AbsListView getAbsListView() {
    	LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	ListView listView = (ListView) layoutInflater.inflate(R.layout.fragment_sale_item_detail, null);
    	return listView;
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(saleItem.getTitle());
    }

	@Override
	protected View getItemView(int position, Message item, View convertView, ViewGroup parent) {
		final Message currentMessage = listItems.get(position);
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.view_message_items, parent, false);
			convertView.setBackgroundResource(R.drawable.selector_transparent_grey);
		}
		TextView text_view_message_sender_name = (TextView) convertView.findViewById(R.id.text_view_message_sender_name);
		TextView text_view_message_date = (TextView) convertView.findViewById(R.id.text_view_message_date);
		TextView text_view_message = (TextView) convertView.findViewById(R.id.text_view_message);
		ImageView image_view_message_sender_icon = (ImageView) convertView.findViewById(R.id.image_view_message_sender_icon);
		
		if (currentMessage.getSender().getUserId().equals(CurrentUser.getCurrentUser().getUserId())) {
			text_view_message_sender_name.setText("Me");
		} else {
			text_view_message_sender_name.setText(currentMessage.getSender().getName());
		}
		
		text_view_message_date.setText(DateUtil.getDateTimeText(currentMessage.getCreatedDate()));
		text_view_message.setText(currentMessage.getMessage());
		
		loadUserIcon(currentMessage, image_view_message_sender_icon);
		
//		convertView.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				User user = currentMessage.getSender();
//				Intent intent = new Intent(getActivity(), DetailActivity.class);
//				Bundle bundle = ActivityBundleFactory.getBundle(user.getUserId(), user, ViewType.USER_ACCOUNT);
//				intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
//				startActivity(intent);
//			}
//		});
		
		return convertView;
	}
	
	private void loadUserIcon(Message message, final ImageView imageView) {
		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
		imageLoader.get(UrlUtil.getImageUrl(message.getSender().getImageId()),
				new ImageLoader.ImageListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						if (getActivity() != null) {
							Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
							imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50),
									SizeUtil.getSizeInPixels(50), bitmap));
						}
					}

					@Override
					public void onResponse(ImageContainer response, boolean isImmediate) {
						if (getActivity() != null) {
							Bitmap bitmap = response.getBitmap();
							if (bitmap == null) {
								bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
							}
							imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50),
									SizeUtil.getSizeInPixels(50), bitmap));
						}
					}
				}
		);
	}
		
	private void getSaleItemFromApi(boolean shouldResultCacheResponse) {
		String url = null;
		url = UrlUtil.getSaleItemUrl(this.saleItem.getItemId());
		messageApiTask = new ApiTaskFactory<SaleItem>().newInstance(getActivity(),
				url, APIMethod.GET, SaleItem.class, new GetMessagesResponseHandler());
		messageApiTask.executeRequest(null, shouldResultCacheResponse);
	}
	
	private class GetMessagesResponseHandler extends ApiResponseHandler<SaleItem> {
		
		@Override
		public void onApiSuccess(SaleItem response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			messageApiTask = null;
			SaleItemDetailsFragment.this.error = null;
			if (isCachedResponse) {
				swipeRefreshLayout.setRefreshing(true);
			} else {
				swipeRefreshLayout.setRefreshing(false);
			}

            if (response == null) {
                error = new MGSError(MGSError.MGSErrorType.API_ERROR, "Sale Item does not exists. Either sold out or rejected.");
            }

            saleItem = response;

			listItems.clear();
			if (saleItem.getMessages() != null) {
				listItems.addAll(response.getMessages());
			}
			update();
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			messageApiTask = null;
			swipeRefreshLayout.setRefreshing(false);
			SaleItemDetailsFragment.this.error = error;
			update();
		}
	}

	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		showView(ContentViewType.CONTENT_LOADING);
		getSaleItemFromApi(true);
	}
	
	@Override
	public void onRefresh() {
		super.onRefresh();
		getSaleItemFromApi(false);
	}

	@Override
	public void onActionPost(String data) {
		MessageRequest message = new MessageRequest(data, null, null, Message.MESSAGE_IN_SALE_ITEM, 
				CurrentUser.getCurrentUser().getUserId(), saleItem.getItemId());
		sendMessageApiTask = new ApiTaskFactory<Message>().newInstance(getActivity(), 
				UrlUtil.getSendMessageUrl(), APIMethod.POST, Message.class, 
					new SendMessageResponseHandler());
		sendMessageApiTask.executeRequest(message, false);
	}

	@Override
	protected View getHeaderView() {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.view_sale_item_detail_header, null);
		return view;
	}
	
	private class SendMessageResponseHandler extends ApiResponseHandler<Message> {
		
		@Override
		public void onApiSuccess(Message response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			if (getActivity() != null) {
				isSaleItemChanged = true;
				saleItem.setModifeidDate(response.getCreatedDate());
				messagePosterView.reset();
                if (saleItem.getMessages() == null) {
                    saleItem.setMessages(new ArrayList<Message>());
                }
                saleItem.getMessages().add(0, response);
                listItems.clear();
				listItems.addAll(saleItem.getMessages());
				// scroll to first comment
				absListView.smoothScrollToPositionFromTop(0, -viewSaleItemCommentSeparator.getTop());
				update();
			}
			MGSApplication.showToast("Comment Added.", Toast.LENGTH_LONG);
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
			if (getActivity() != null) {
				messagePosterView.hideLoading();
			}
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.sale_item_detail, menu);
		// TODO enable edit
		menu.removeItem(R.id.action_edit_sale_item);
		
		// disable edit, sold if user is not seller
		if (!saleItem.getSeller().getUserId().equals(CurrentUser.getCurrentUser().getUserId())) {
			menu.removeItem(R.id.action_edit_sale_item);
			menu.removeItem(R.id.action_mark_sale_item_sold);
		}

		// disable activate if current user is not admin or moderator
		if (CurrentUser.getCurrentUser().getUserRole() != User.USER_ROLE_ADMIN 
				&& CurrentUser.getCurrentUser().getUserRole() != User.USER_ROLE_MODERATOR) {
			menu.removeItem(R.id.action_activate_sale_item);
			menu.removeItem(R.id.action_reject_sale_item);
			menu.removeItem(R.id.action_block_user_account);
		}
		
		if (saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_SOLD) {
			menu.removeItem(R.id.action_mark_sale_item_sold);
			menu.removeItem(R.id.action_activate_sale_item);
		}
		
		if (saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_OPEN) {
			menu.removeItem(R.id.action_activate_sale_item);
		}
		
		if (saleItem.getItemStatus() == SaleItem.SALE_ITEM_STATUS_REJECTED) {
			menu.removeItem(R.id.action_reject_sale_item);
			menu.removeItem(R.id.action_activate_sale_item);
		}

		// update alert watch icon
   		if (saleItem.getCategories() == null) {
			menu.removeItem(R.id.action_add_to_alerts);
		} else {
			if (alertKeywordsList != null) {
				int count = saleItem.getCategories().size();
				for (String tag : saleItem.getCategories()) {
					for (AlertKeyword alertKeyword : alertKeywordsList) {
						if (tag.equals(alertKeyword.getKeywordName())) {
							count--;
							break;
						}
					}
				}
				if (count > 0) {
					isKeywordAlreadyAddedForAlert = true;
				} else {
					isKeywordAlreadyAddedForAlert = false;
				}
			}
			MenuItem menuItem = menu.findItem(R.id.action_add_to_alerts);
			if (isKeywordAlreadyAddedForAlert) {
				menuItem.setIcon(R.drawable.ic_visibility_white_24dp);
			} else {
				menuItem.setIcon(R.drawable.ic_visibility_off_white_24dp);
			}
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
			case R.id.action_edit_sale_item:
				break;
			case R.id.action_mark_sale_item_sold:
				soldSaleitem();
				break;
			case R.id.action_activate_sale_item:
				activateSaleItem();
				break;
			case R.id.action_reject_sale_item:
				rejectSaleItem();
				break;
			case R.id.action_block_user_account:
				blockUser();
				break;
			case R.id.action_add_to_alerts:
				updateKeywordForAlerts();
				break;
		}
		return result;
	}

	private void updateKeywordForAlerts() {
		if (saleItem.getCategories() == null || saleItem.getCategories().isEmpty()) {
			MGSApplication.showToast("Unable to add tag(s) in watch list.", Toast.LENGTH_LONG);
			return;
		}
		String url = null;
		if (isKeywordAlreadyAddedForAlert) {
			url = UrlUtil.getAddAlertKeywordsUrl();
		} else {
			url = UrlUtil.getDeleteAlertKeywordsUrl();
		}
		final ArrayList<AlertKeyword> alertKeywords = new ArrayList<>();
		for (String tag : saleItem.getCategories()) {
			AlertKeyword alertKeyword = new AlertKeyword();
			alertKeyword.setKeywordName(tag);
			alertKeyword.setKeywordType(AlertKeyword.ALERT_KEYWORD_TYPE_TAG);
			alertKeywords.add(alertKeyword);
		}

		final ProgressDialog progressDialog = new ProgressDialog(getActivity());
		final boolean isAddKeywordForAlertFinal = this.isKeywordAlreadyAddedForAlert;
		String progressTitle = null;
		if (isAddKeywordForAlertFinal) {
			progressTitle = "adding tag(s) in watch list..";
		} else {
			progressTitle = "removing tag(s) from watch list..";
		}
		progressDialog.setMessage(progressTitle);

		ApiTask<SuccessResponse> apiTask = new ApiTaskFactory<SuccessResponse>().newInstance(getActivity(), url, APIMethod.POST, SuccessResponse.class, new ApiResponseHandler<SuccessResponse>() {

			@Override
			public void onApiFailure(MGSError error) {
				super.onApiFailure(error);
				if (isAddKeywordForAlertFinal) {
					MGSApplication.showToast("Unable to add tag(s) in watch list.", Toast.LENGTH_LONG);
				} else {
					MGSApplication.showToast("Unable to remove tag(s) from watch list.", Toast.LENGTH_LONG);
				}
				progressDialog.dismiss();
			}

			@Override
			public void onApiSuccess(SuccessResponse response, boolean isCachedResponse) {
				super.onApiSuccess(response, isCachedResponse);
				if (isAddKeywordForAlertFinal) {
					DatabaseProvider.getInstance().addAlertKeywords(alertKeywords);
					alertKeywordsList.addAll(alertKeywords);
					MGSApplication.showToast("Tag(s) added in watch list.", Toast.LENGTH_LONG);
				} else {
					DatabaseProvider.getInstance().deleteAlertKeywords(alertKeywords);
					alertKeywordsList.removeAll(alertKeywords);
					MGSApplication.showToast("Tag(s) removed from watch list.", Toast.LENGTH_LONG);
				}
				progressDialog.dismiss();
				getActivity().invalidateOptionsMenu();
			}
		});
		AlertKeywordsRequest alertKeywordsRequest = new AlertKeywordsRequest();
		alertKeywordsRequest.setAlertKeywords(alertKeywords);
		apiTask.executeRequest(alertKeywordsRequest, false);
		progressDialog.show();
	}
	
	@Override
	public void onClick(View view) {
		if (view == this.buttonAddComment) {
			// move to add comment footer view
			absListView.smoothScrollToPositionFromTop(listItems.size(), 0);
			messagePosterView.requestEditViewFocus();
		} else if (view == this.imageViewSellerIcon) {
			Intent intent = new Intent(getActivity(), DetailActivity.class);
			Bundle bundle = ActivityBundleFactory.getBundle(saleItem.getSeller().getUserId(), saleItem.getSeller(), 
					ViewType.USER_ACCOUNT);
			intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
			startActivity(intent);
		} else if (view == this.layoutSeller) {
			Intent intent = new Intent(getActivity(), DetailActivity.class);
			Bundle bundle = ActivityBundleFactory.getBundle(saleItem.getSeller().getUserId(), 
					saleItem.getSeller(), ViewType.MESSAGE_BETWEEN_USER);
			intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
			startActivity(intent);
		} 
//			else if (view == this.textViewSallerMobile) {
//			Intent intent = new Intent(Intent.ACTION_DIAL);
//			intent.setData(Uri.parse("tel:" + saleItem.getSeller().getMobileNumber()));
//			startActivity(Intent.createChooser(intent, "Make call using.."));
//		}
	}
	
	@Override
	public boolean onActivityBackPressed() {
		if (isSaleItemChanged) {
			getActivity().setResult(Activity.RESULT_OK);
		}
		this.getActivity().finish();
		return true;
	}
	
	private void activateSaleItem() {
		String title = "Approve Item";
		String message = "Do you want to approve this sale item?";
		String positiveButton = "APPROVE";
		String url = UrlUtil.getActivateSaleItemUrl(saleItem.getItemId());
		showConfirmationDialog(title, message, positiveButton, url, ActionType.ACTIVATE_SALE_ITEM);
	}

	private void rejectSaleItem() {
		String title = "Reject Item";
		String message = "Do you want to reject this sale item?";
		String positiveButton = "REJECT";
		String url = UrlUtil.getRejectSaleItemUrl(saleItem.getItemId());
		showConfirmationDialog(title, message, positiveButton, url, ActionType.REJECT_SALE_ITEM);
	}
	
	private void soldSaleitem() {
		String title = "Sold Item";
		String message = "Do you want to marked this item as sold?";
		String positiveButton = "SOLD";
		String url = UrlUtil.getMarkAsSoldSaleItemUrl(saleItem.getItemId());
		showConfirmationDialog(title, message, positiveButton, url, ActionType.SOLD_SALE_ITEM);
	}
	
	private void blockUser() {
		String title = "Block User";
		String message = "Do you want to block this user account?";
		String positiveButton = "BLOCK";
		String url = UrlUtil.getBlockUserUrl(saleItem.getSeller().getUserId());
		showConfirmationDialog(title, message, positiveButton, url, ActionType.BLOCK_USER_ACCOUNT);
	}
	
	private void showConfirmationDialog(final String title, final String message, final String positiveButton, final String url, 
			final ActionType actionType) {
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ApiTask<SuccessResponse> apiTask = new ApiTaskFactory<SuccessResponse>().newInstance(getActivity(), 
						url, APIMethod.GET, 
							SuccessResponse.class, new SoldSaleItemResponseHandler(actionType));
				apiTask.executeRequest(null, false);
				MGSApplication.showToast(getStartMessage(actionType), Toast.LENGTH_SHORT);
			}
		});
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
			
	}
	
	private String getCategoriesText() {
		StringBuffer text = new StringBuffer();
		if (saleItem.getCategories() != null) {
			for (String categoryNmae : saleItem.getCategories()) {
				text.append(categoryNmae + ", ");
			}
		}
		// remove last ,
		try {
			text.delete(text.length() - 2, text.length() - 1);
		} catch(Exception e) {
			if (Logger.DEBUG) {
				e.printStackTrace();
			}
		}
		return text.toString().trim();
	}
	
	private void loadAllImages() {
		// add images view to display multiple images
		if (saleItem.getImages() != null && !saleItem.getImages().isEmpty()) {
			// create image views equal to no of urls
			int childRequire = saleItem.getImages().size() - layoutImagesList.getChildCount();
			for (int i = 0; i < childRequire; i++) {
				ImageView imageView = getImagesView();
				layoutImagesList.addView(imageView);
			}
			
			for (int i = 0; i < saleItem.getImages().size(); i++) {
				ImageView imageView = (ImageView) layoutImagesList.getChildAt(i);
				loadImage(imageView, saleItem.getImages().get(i));
				final int currentIndex = i;
				imageView.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						// create list of images url instead of only id
						ArrayList<String> imageUrls = new ArrayList<>(saleItem.getImages().size());
						for (String imageId : saleItem.getImages()) {
							imageUrls.add(UrlUtil.getImageUrl(imageId));
						}
						Intent intent = new Intent(getActivity(), FullScreenActivity.class);
						Bundle bundle = new Bundle();
						bundle.putStringArrayList(ImagesFragment.KEY_IMAGE_URLS, imageUrls);
						bundle.putInt(ImagesFragment.KEY_CURRENT_IMAGE_INDEX, currentIndex);
						bundle.putSerializable(ActivityBundleFactory.KEY_FRAGMENT_CLASS_NAME, ImagesFragment.class);
						intent.putExtra(FullScreenActivity.KEY_FULL_SCREEN_ACTIVITY_BUNDLE, bundle);
						startActivity(intent);
					}
				});
			}
		} else {
			this.layoutSaleItemsImagesContainer.setVisibility(View.GONE);
		}	
	}
	
	private void loadImage(final ImageView imageView, String imageId) {
		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
		imageLoader.get(UrlUtil.getImageUrl(imageId), new ImageLoader.ImageListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if (getActivity() != null) {
						imageView.setVisibility(View.GONE);
					}
				}
				
				@Override
				public void onResponse(ImageContainer response, boolean isImmediate) {
					if (getActivity() != null) {
						imageView.setVisibility(View.VISIBLE);
						imageView.setImageBitmap(response.getBitmap());
					}
				}
			});
	}
	
	private void loadUserIcon() {
		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
		imageLoader.get(UrlUtil.getImageUrl(saleItem.getSeller().getImageId()), new ImageLoader.ImageListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if (getActivity() != null) {
						Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
						imageViewSellerIcon.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
								SizeUtil.getSizeInPixels(50), bitmap));
					}
				}
				
				@Override
				public void onResponse(ImageContainer response, boolean isImmediate) {
					if (getActivity() != null) {
						Bitmap bitmap = response.getBitmap();
						if (bitmap == null) {
							bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
						}
						imageViewSellerIcon.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
								SizeUtil.getSizeInPixels(50), bitmap));
					}
				}
			});
	}
	
	private String getStartMessage(ActionType actionType) {
		switch (actionType) {
			case ACTIVATE_SALE_ITEM:
				return "Approving";
			case REJECT_SALE_ITEM:
				return "Rejecting";
			case SOLD_SALE_ITEM:
				return "Marking Sold";
			case BLOCK_USER_ACCOUNT:
				return "Blocking User Account";
		}
		return "";
	}
	
	private String getSuccessMessage(ActionType actionType) {
		switch (actionType) {
			case ACTIVATE_SALE_ITEM:
				return "Sale Item Approved";
			case REJECT_SALE_ITEM:
				return "Sale Item Rejected";
			case SOLD_SALE_ITEM:
				return "Sale Item Marked as Sold";
			case BLOCK_USER_ACCOUNT:
				return "User Account Blocked";
		}
		return "";
	}
	
	private enum ActionType {
		ACTIVATE_SALE_ITEM, REJECT_SALE_ITEM, SOLD_SALE_ITEM, BLOCK_USER_ACCOUNT
	}
	
	private ImageView getImagesView() {
		ImageView imageView = new ImageView(getActivity());
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.WRAP_CONTENT, 
				(int) getResources().getDimension(R.dimen.sale_items_image_height_detail_view));
		layoutParams.setMargins(0, 0, (int) getResources().getDimension(R.dimen.post_fragment_margin), 0);
		imageView.setLayoutParams(layoutParams);
		imageView.setAdjustViewBounds(true);
		imageView.setBackgroundResource(R.drawable.bg_card_image);
		imageView.setScaleType(ScaleType.FIT_CENTER);
		int currentWidthPixels = getResources().getDisplayMetrics().widthPixels - SizeUtil.getSizeInPixels(20);
		imageView.setMaxWidth(currentWidthPixels);
		return imageView;
	}

	/** Message to display if this item status is not open. */
	private String getItemStatusNotOpenMessage() {
		switch (saleItem.getItemStatus()) {
			case SaleItem.SALE_ITEM_STATUS_PENDING:
				return "This item has not been approved.";
			case SaleItem.SALE_ITEM_STATUS_REJECTED:
				return "This item has been rejected.";
			case SaleItem.SALE_ITEM_STATUS_SOLD:
				return "This item has been sold.";
		}
		return null;
	}
	
	private class SoldSaleItemResponseHandler extends ApiResponseHandler<SuccessResponse> {
		
		ActionType actionType;
		
		public SoldSaleItemResponseHandler(ActionType actionType) {
			this.actionType = actionType;
		}
		
		@Override
		public void onApiSuccess(SuccessResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			isSaleItemChanged = true;
			MGSApplication.showToast(getSuccessMessage(actionType), Toast.LENGTH_LONG);
			if (actionType == ActionType.ACTIVATE_SALE_ITEM) {
				saleItem.setItemStatus(SaleItem.SALE_ITEM_STATUS_OPEN);
			} else if (actionType == ActionType.REJECT_SALE_ITEM) {
				saleItem.setItemStatus(SaleItem.SALE_ITEM_STATUS_REJECTED);
			} else if (actionType == ActionType.SOLD_SALE_ITEM) {
				saleItem.setItemStatus(SaleItem.SALE_ITEM_STATUS_SOLD);
			}
			getActivity().invalidateOptionsMenu();
			setSaleItemStatusText();
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
		}
	}

}
