package com.ms.mygaragesale.model;

import com.ms.mygaragesale.core.util.StringUtil;
import com.ms.mygaragesale.model.response.Response;


public class User extends Response implements BaseItem {
	
private static final long serialVersionUID = 4564685745390188288L;
	
	public static final int USER_ROLE_ADMIN = 1;
	public static final int USER_ROLE_USER = 3;
	public static final int USER_ROLE_MODERATOR = 2;
	
	public static final int ACCOUNT_STATUS_ACTIVE = 1;
	public static final int ACCOUNT_STATUS_PENDING = 2;
	public static final int ACCOUNT_STATUS_UPGRADE_PENDING = 3;
	public static final int ACCOUNT_STATUS_DOWNGRADE_PENDING = 4;
	public static final int ACCOUNT_STATUS_BLOCKED = 5;
	
	public static final int ACCOUNT_TYPE_SERVER = 1;
	public static final int ACCOUNT_TYPE_FACEBOOK = 2;
	public static final int ACCOUNT_TYPE_TWITTER = 3;
	
    protected String userId;
    protected String username;
    protected String name;
    protected String emailId;
//    private String emailIdAlternate;
    protected String mobileNumber;
//    private String mobileNumberAlternate;
    protected String address;
    protected String imageId;
    protected Integer userRole;
    protected Integer accountStatus;
    protected Integer accountType;
    
//    private static CurrentUser currentUser;
	
//	public static CurrentUser getCurrentUser() {
//		if (currentUser == null) {
//			currentUser = DatabaseProvider.getInstance().getCurrentUser();
//		}
//		return currentUser;
//	}
	
//	public void updateCurrentUser() {
//		Context context = MGSApplication.getInstance().getApplicationContext();
//		DatabaseProvider.getInstance().updateCurrentUser(currentUser);
//	}

    public User() {
    }

//    public User(Long id) {
//        this.id = id;
//    }

//    public User(Long id, String userId, String username, String name, String emailId, String mobileNumber, 
//    		String address, String location, String imageId, Integer userRole, Integer accountStatus, 
//    			Integer accountType) {
//        this.id = id;
//        this.userId = userId;
//        this.username = username;
//        this.name = name;
//        this.emailId = emailId;
////        this.emailIdAlternate = emailIdAlternate;
//        this.mobileNumber = mobileNumber;
////        this.mobileNumberAlternate = mobileNumberAlternate;
//        this.address = address;
//        this.location = location;
//        this.imageId = imageId;
//        this.userRole = userRole;
//        this.accountStatus = accountStatus;
//        this.accountType = accountType;
//    }
//    
//    public User(com.ms.mygaragesale.core.db.dao.UserTable dbUser) {
//    	this.id = dbUser.getId();
//        this.userId = dbUser.getUserId();
//        this.username = dbUser.getUsername();
//        this.name = dbUser.getName();
//        this.emailId = dbUser.getEmailId();
//        this.mobileNumber = dbUser.getMobileNumber();
//        this.address = dbUser.getAddress();
//        this.location = dbUser.getLocation();
//        this.imageId = dbUser.getImageId();
//        this.userRole = dbUser.getUserRole();
//        this.accountStatus = dbUser.getAccountStatus();
//        this.accountType = dbUser.getAccountType();
//    }
//    
//    public com.ms.mygaragesale.core.db.dao.UserTable getDbUser() {
//		com.ms.mygaragesale.core.db.dao.UserTable dbUser = new com.ms.mygaragesale.core.db.dao.UserTable(id, userId, username, 
//				name, emailId, mobileNumber, address, location, userRole, accountStatus, accountType, imageId);
//		return dbUser;
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

//    public String getEmailIdAlternate() {
//        return emailIdAlternate;
//    }
//
//    public void setEmailIdAlternate(String emailIdAlternate) {
//        this.emailIdAlternate = emailIdAlternate;
//    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

//    public String getMobileNumberAlternate() {
//        return mobileNumberAlternate;
//    }
//
//    public void setMobileNumberAlternate(String mobileNumberAlternate) {
//        this.mobileNumberAlternate = mobileNumberAlternate;
//    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String image) {
        this.imageId = image;
    }

    public Integer getUserRole() {
        return userRole;
    }

    public void setUserRole(Integer userRole) {
        this.userRole = userRole;
    }

    public Integer getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(Integer accountStatus) {
        this.accountStatus = accountStatus;
    }

    public Integer getAccountType() {
        return accountType;
    }

    public void setAccountType(Integer accountType) {
        this.accountType = accountType;
    }
       
    public void copyNotNullValueWithUser(User user) {
		if (!StringUtil.isNullOrEmpty(user.address)) {
			this.address = user.address;
		}
		if (!StringUtil.isNullOrEmpty(user.emailId)) {
			this.emailId = user.emailId;
		}
		if (!StringUtil.isNullOrEmpty(user.mobileNumber)) {
			this.mobileNumber = user.mobileNumber;
		}
		if (!StringUtil.isNullOrEmpty(user.name)) {
			this.name = user.name;
		}
		if (!StringUtil.isNullOrEmpty(user.username)) {
			this.username = user.username;
		}
		if (user.accountStatus != 0) {
			this.accountStatus = user.accountStatus;
		}
		if (user.accountType != 0) {
			this.accountType = user.accountType;
		}
		if (user.userRole != 0) {
			this.userRole = user.userRole;
		}
		if (!StringUtil.isNullOrEmpty(user.imageId)) {
			this.imageId = user.imageId;
		}
		if (!StringUtil.isNullOrEmpty(user.userId)) {
			this.userId = user.userId;
		}
	}

}
