package com.ms.mygaragesale.ui.frag;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.Toast;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.DefaultConf;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.util.ImageUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.ui.IActivityEventCallback;
import com.ms.mygaragesale.ui.frag.LoadingMessageDialogFragment.DialogFragmentCompleteListener;
import com.ms.mygaragesale.ui.view.ActionImageLayout;
import com.ms.mygaragesale.ui.view.ActionImageLayout.OnImageActionButtonListener;
import com.ms.mygaragesale.ui.view.FloatingActionButton;
import com.soundcloud.android.crop.Crop;

public class AddNewSaleItemFragment extends Fragment implements OnClickListener, IActivityEventCallback, DialogFragmentCompleteListener {

	public static final String KEY_CREATE_SPONSORED_ITEM = "KEY_CREATE_SPONSORED_ITEM";

	protected FloatingActionButton buttonAddImage;
	protected GridView gridViewImagePicked;
	
	protected EditText editTextPostTitle;
	protected EditText editTextPostDescription;
	protected EditText editTextPostPrice;
	protected EditText editTextPostCategory;
	
	private ImagePickedAdapter imagePickedAdapter;
	protected ArrayList<String> listImagePaths;
//	protected CharSequence[] listCategoriesName;
	protected SaleItem saleItem;
	private boolean createSponsoredItem;
	
	private ResizeImageAsyncTask resizeImageAsyncTask;
	
	protected boolean canSaleNewItem;

	public static AddNewSaleItemFragment newInstance(boolean createSponsoredItem) {
		final AddNewSaleItemFragment fragment = new AddNewSaleItemFragment();
		Bundle bundle = new Bundle();
		bundle.putBoolean(KEY_CREATE_SPONSORED_ITEM, createSponsoredItem);
		fragment.setArguments(bundle);
		return fragment;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setRetainInstance(true);
		this.setHasOptionsMenu(true);

		Bundle bundle = getArguments();
		createSponsoredItem = bundle.getBoolean(KEY_CREATE_SPONSORED_ITEM);
		
		// delete all earlier cache images
		DefaultConf.deleteAllImageCaches();
		
		listImagePaths = new ArrayList<>();
		imagePickedAdapter = new ImagePickedAdapter();
		Logger.log(getResources().getConfiguration().smallestScreenWidthDp +" much width");
		
		// get categories from database
//		ArrayList<Category> categories = DatabaseProvider.getInstance().getCategories();
//		Collections.sort(categories);
//		listCategoriesName = new CharSequence[categories.size()];
//		for (int i = 0; i < categories.size(); i++) {
//			listCategoriesName[i] = categories.get(i).getCategoryName();
//		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_create_sale_item, container, false);
		this.buttonAddImage = (FloatingActionButton) rootView.findViewById(R.id.action_button_add_image);
		this.editTextPostTitle = (EditText) rootView.findViewById(R.id.edit_text_post_title);
		this.editTextPostDescription = (EditText) rootView.findViewById(R.id.edit_text_post_description);
		this.editTextPostPrice = (EditText) rootView.findViewById(R.id.edit_text_post_price);	
		this.editTextPostCategory = (EditText) rootView.findViewById(R.id.edit_text_post_categories);
		this.gridViewImagePicked = (GridView) rootView.findViewById(R.id.grid_view_image_picked);
		this.initialize();
		return rootView;
	}

	
	@Override
	public void onDestroy() {
		super.onDestroy();
		updateLinkToDialogFragment(false);
		if (resizeImageAsyncTask != null) {
			resizeImageAsyncTask.cancel(true);
		}
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.setTitle();
	}
	
	protected void setTitle() {
		final ActionBar actionBar = ((ActionBarActivity) this.getActivity()).getSupportActionBar();
		String title = "Sell New Item";
		if (createSponsoredItem) {
			title = "Create Sponsored Ad Post";
		}
		actionBar.setTitle(title);
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add_new_sale_item, menu);
		MenuItem menuItem = menu.findItem(R.id.action_create_sale_item);
		if (canSaleNewItem) {
			menuItem.getIcon().setAlpha(255);
			menuItem.setEnabled(true);
		} else {
			menuItem.getIcon().setAlpha(100);
			menuItem.setEnabled(false);
		}
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		boolean result = super.onOptionsItemSelected(item);
		if (item.getItemId() == R.id.action_create_sale_item) {
			if (canSaleNewItem) {
				canSaleNewItem = false;
				getActivity().invalidateOptionsMenu();
				createNewSaleItem();
			}
			result = true;
		}
		return result;
	}
	
	@Override
	public void onClick(View view) {
		if (view == this.buttonAddImage) {
			Intent intent = new Intent(Intent.ACTION_GET_CONTENT).setType("image/*");
			try {
				this.startActivityForResult(intent, Crop.REQUEST_PICK);
			} catch (ActivityNotFoundException e) {
				MGSApplication.showToast(getResources().getString(R.string.crop__pick_error), Toast.LENGTH_SHORT);
			}
		} 
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent result) {
		if (requestCode == Crop.REQUEST_PICK && resultCode == Activity.RESULT_OK) {
			this.beginCrop(result.getData());
		} else if (requestCode == Crop.REQUEST_CROP) {
			this.handleCrop(resultCode, result);
		}
	}
	
	protected void initialize() {
		this.buttonAddImage.setOnClickListener(this);
		this.gridViewImagePicked.setAdapter(imagePickedAdapter);
		this.editTextPostTitle.addTextChangedListener(new EditTextWatcher());
		this.editTextPostDescription.addTextChangedListener(new EditTextWatcher());
		this.editTextPostPrice.addTextChangedListener(new EditTextWatcher());
		this.editTextPostCategory.addTextChangedListener(new EditTextWatcher());
		this.updateLinkToDialogFragment(true);
	}
	
	private void updateLinkToDialogFragment(boolean enable) {
		Fragment prev = getFragmentManager().findFragmentByTag("dialog");
	    if (prev != null && prev instanceof AddNewSaleItemDialogFragment) {
	    	DialogFragmentCompleteListener listener = this;
	    	if (!enable) {
				listener = null;
			}
	    	((AddNewSaleItemDialogFragment) prev).setDialogFragmentCompleteListener(listener);
	    }
	}
	
	private void createNewSaleItem() {
		ArrayList<String> images = new ArrayList<>(listImagePaths.size());
		for (String filePath : listImagePaths) {
			images.add(filePath);
		}

		this.saleItem = new SaleItem(null, editTextPostTitle.getText().toString(), 
				editTextPostDescription.getText().toString(), Integer.parseInt(editTextPostPrice.getText().toString()),
					new Date(), new Date(), SaleItem.SALE_ITEM_STATUS_PENDING, CurrentUser.getCurrentUser(), 
						images, null, CurrentUser.getCurrentUser().getLocation(), this.getSaleItemSponsored());
		this.saleItem.setCategories(editTextPostCategory.getText().toString());
		this.saleItem.setImages(images);
		showCreateNewSaleItemDialog();		
	}
	
	protected int getSaleItemSponsored() {
		if (createSponsoredItem) {
			return SaleItem.SALE_ITEM_SPONSORED;
		}
		return SaleItem.SALE_ITEM_NOT_SPONSORED;
	}
	
	private void showDiscardDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle("Discard Changes");
		builder.setMessage("Do you want to discard the changes?");
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		builder.setPositiveButton("DISCARD", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				// delete current image files from cache
				deleteImageCaches();
				getActivity().finish();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
		
	}
	
	private void beginCrop(Uri source) {
		Uri outputUri = Uri.parse(DefaultConf.getImageCachePath());
		Crop crop = new Crop(source).output(outputUri).asSquare().withMaxSize(DefaultConf.IMAGE_MAX_WIDTH, DefaultConf.IMAGE_MAX_HEIGHT);
		this.startActivityForResult(crop.getIntent(this.getActivity()), Crop.REQUEST_CROP);
	}

	private void handleCrop(int resultCode, Intent result) {
		if (resultCode == Activity.RESULT_OK) {
			String outPath = Crop.getOutput(result).toString();
			
			resizeImageAsyncTask = new ResizeImageAsyncTask();
			resizeImageAsyncTask.execute(outPath);
			
		} else if (resultCode == Crop.RESULT_ERROR) {
			MGSApplication.showToast(Crop.getError(result).getMessage(), Toast.LENGTH_SHORT);
		}
	}
	
	private void updateActionButton() {
		if (this.listImagePaths.size() >= 3) {
			this.buttonAddImage.setVisibility(View.INVISIBLE);
		} else {
			this.buttonAddImage.setVisibility(View.VISIBLE);
		}
		updateCanSave();
	}
	
	private boolean isDataChanged() {
		if (editTextPostTitle.getText().toString().length() > 0) {
			return true;
		}
		if (editTextPostPrice.getText().toString().length() > 0) {
			return true;
		}
		if (editTextPostDescription.getText().toString().length() > 0) {
			return true;
		}
		if (listImagePaths.size() > 0) {
			return true;
		} 
		return false;
	}
	
	protected void updateCanSave() {
		boolean oldCanSaveValue = canSaleNewItem;
		if (editTextPostTitle.getText().toString().trim().length() < 4) {
			editTextPostTitle.setError("length must me greater than 3");
		} else {
			editTextPostTitle.setError(null);
		}
		if (createSponsoredItem && editTextPostTitle.getText().toString().length() >= 4
				&& editTextPostPrice.getText().toString().length() > 0
				&& editTextPostDescription.getText().toString().length() > 0
				&& editTextPostCategory.getText().toString().length() > 0
				&& !listImagePaths.isEmpty()) {
			canSaleNewItem = true;
		} else if (!createSponsoredItem && editTextPostTitle.getText().toString().length() >= 4
				&& editTextPostPrice.getText().toString().length() > 0
				&& editTextPostDescription.getText().toString().length() > 0
				&& !listImagePaths.isEmpty()) {
			canSaleNewItem = true;
		} else {
			canSaleNewItem = false;
		}
		if (oldCanSaveValue != canSaleNewItem) {
			getActivity().invalidateOptionsMenu();
		}
	}
	
	private void deleteImageCaches() {
		for (String filePath : listImagePaths) {
			this.deleteImagePath(filePath);
		}
	}
	
	private void deleteImagePath(String filePath) {
		File file = new File(filePath);
		file.delete();
	}
	
	
	private class ImagePickedAdapter extends BaseAdapter implements OnImageActionButtonListener {

		@Override
		public int getCount() {
			return listImagePaths.size();
		}

		@Override
		public Object getItem(int arg0) {
			return listImagePaths.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			return 0;
		}

		@Override
		public View getView(int position, View view, ViewGroup parent) {
			if (view == null) {
				LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
				view = layoutInflater.inflate(R.layout.view_image_picked_items, null);
			}
			ActionImageLayout actionImageLayout = (ActionImageLayout) view;
			actionImageLayout.setImageUri(Uri.parse(listImagePaths.get(position)));
			actionImageLayout.setOnImageActionButtonListener(this);
			return view;
		}

		@Override
		public void onImageActionClick(ActionImageLayout actionImageLayout) {
			String imagePath = actionImageLayout.getImageUri().toString();
			listImagePaths.remove(imagePath);
			deleteImagePath(imagePath);
			imagePickedAdapter.notifyDataSetChanged();
			updateActionButton();
		}
	}
	
	private class EditTextWatcher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable arg0) {
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before, int count) {
			updateCanSave();
		}
	}
	
	private void showCreateNewSaleItemDialog() {
		AddNewSaleItemDialogFragment addNewSaleItemDialogFragment = AddNewSaleItemDialogFragment.newInstance(saleItem);
		addNewSaleItemDialogFragment.setDialogFragmentCompleteListener(this);
		FragmentManager fragmentManager = getFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		Fragment prev = fragmentManager.findFragmentByTag("dialog");
	    if (prev != null) {
	    	transaction.remove(prev);
	    }
	    transaction.addToBackStack(null);
	    addNewSaleItemDialogFragment.show(transaction, "dialog");
	}

	@Override
	public boolean onActivityBackPressed() {
		if (isDataChanged()) {
			showDiscardDialog();
		} else {
			this.getActivity().finish();
		}
		return true;
	}

	@Override
	public void onComplete(MGSError error) {
		if (error == null) {
			this.getActivity().finish();
		}
	}	
	
	private class ResizeImageAsyncTask extends AsyncTask<String, Void, String> {
		
		private ProgressDialog progressDialog;
		
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			progressDialog = new ProgressDialog(getActivity());
			progressDialog.setMessage("Resizing picture...");
			progressDialog.show();
		}
		
		@Override
		protected String doInBackground(String... params) {
			String imageUrl = null;
			String inputImageUrl = params[0];
			try {
				imageUrl = ImageUtil.compressImage(inputImageUrl);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return imageUrl;
		}
		
		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			progressDialog.dismiss();
			if (result == null) {
				MGSApplication.showToast("Unable to resize file, try again.", Toast.LENGTH_LONG);
			} else {
				Logger.log("output path: " + result);
				listImagePaths.add(result);
				imagePickedAdapter.notifyDataSetChanged();
				updateActionButton();
			}
		}
		
	}
}
