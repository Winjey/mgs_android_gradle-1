package com.ms.mygaragesale.core.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.widget.Toast;

import com.facebook.Session;
import com.ms.mygaragesale.core.Constants;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.response.SuccessResponse;
import com.ms.mygaragesale.ui.AuthenticationActivity;

public class LogoutUtility {

	public static void processLogout(final Context context) {
		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setTitle("Logout");
		builder.setMessage("Do you want to log out?");
		builder.setPositiveButton("LOGOUT", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				ApiTask<SuccessResponse> apiTask = new ApiTaskFactory<SuccessResponse>().newInstance(context,
						UrlUtil.getLogoutUrl(CurrentUser.getCurrentUser().getUserId()), APIMethod.GET,
						SuccessResponse.class, null);
				apiTask.executeRequest(null, false);

				MGSApplication.showToast("Logged out.", Toast.LENGTH_LONG);

				// logout from fb
				UnauthorisedUtility.processUnauthentication();
			}
		});
		builder.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		AlertDialog alertDialog = builder.create();
		alertDialog.show();
	}
	
	// logging out without promoting user
	public static void processLogoutInstantly(final Context context) {
		ApiTask<SuccessResponse> apiTask = new ApiTaskFactory<SuccessResponse>().newInstance(context, 
						UrlUtil.getLogoutUrl(CurrentUser.getCurrentUser().getUserId()), APIMethod.GET, 
							SuccessResponse.class, null);
		apiTask.executeRequest(null, false);
		CurrentUser currentUser = CurrentUser.getCurrentUser();
		currentUser.deleteCurrentUser();
		// TODO delete all cache files
		// delete data from db
		
		// logged out from fb session
		Session session = Session.getActiveSession();
		if (session == null) {
			session = Session.openActiveSessionFromCache(MGSApplication.getInstance().getApplicationContext());
			if (session != null) {
				session.closeAndClearTokenInformation();
			}
		}
		
		Intent intent = new Intent(context, AuthenticationActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
		context.startActivity(intent);
		// start broadcasting for closing all activity in case of logout
		Intent closeActivity = new Intent(Constants.CLOSE_ALL_ACTIVITY_LOGOUT_ACTION);
		context.sendBroadcast(closeActivity);
		
		//delete all cache
		VolleyRequestSingleton.getInstance(context).getRequestQueue().getCache().clear();
	}
}
