package com.ms.mygaragesale.ui;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.Constants;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.util.NotificationUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.MenuItemChild;
import com.ms.mygaragesale.model.MenuItemGroup;
import com.ms.mygaragesale.model.SaleItem;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.frag.AccountPendingRequestsListFragment;
import com.ms.mygaragesale.ui.frag.AlertKeywordsListFragment;
import com.ms.mygaragesale.ui.frag.ConversationsListFragment;
import com.ms.mygaragesale.ui.frag.NavigationDrawerFragment;
import com.ms.mygaragesale.ui.frag.SaleItemsFragment;
import com.ms.mygaragesale.ui.frag.SaleItemsFragment.SaleItemViewType;
import com.ms.mygaragesale.ui.frag.SettingFragment;
import com.ms.mygaragesale.ui.frag.SponsoredSaleItemsFragment;

import java.util.ArrayList;

public class MainActivity extends MGSActivity
	implements NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.activity_main);

		this.mNavigationDrawerFragment = (NavigationDrawerFragment)
				this.getFragmentManager().findFragmentById(R.id.navigation_drawer);
		this.mTitle = this.getTitle();

		// Set up the drawer.
		this.mNavigationDrawerFragment.setUp(
				R.id.navigation_drawer,
				(DrawerLayout) this.findViewById(R.id.drawer_layout));

		this.handleDeepLink(getIntent());
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putCharSequence("TITLE", mTitle);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		mTitle = savedInstanceState.getCharSequence("TITLE");
	}

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        this.handleDeepLink(intent);
    }

	/** This method handle deep link, it also update */
	private void handleDeepLink(Intent intent) {
		if (intent != null) {
			Bundle data = intent.getBundleExtra(NotificationUtil.KEY_NOTIFICATION_BUNDLE);
			if (data != null) {
				int notificationType = data.getInt(NotificationUtil.KEY_NOTIFICATION_TYPE);
				int notificationId = data.getInt(NotificationUtil.KEY_NOTIFICATION_ID);
				// open sale item detail
				if (notificationType == NotificationUtil.NOTIFICATION_TYPE_SALE_ITEM_ALERT) {
					this.mNavigationDrawerFragment.selectMenuItem(MenuItemGroup.ITEM_ALERTS, MenuItemChild.ALERT_KEYWORDS);
					Intent newIntent = new Intent(this, DetailActivity.class);
					SaleItem saleItem = (SaleItem) data.getSerializable(NotificationUtil.KEY_NOTIFICATION_SALE_ITEM_OBJECT);
					Bundle bundle = ActivityBundleFactory.getBundle(null, saleItem, ViewType.SALE_ITEM_DETAIL);
					// TODO: find out way to do get bundle in more clear way
					bundle.putInt(NotificationUtil.KEY_NOTIFICATION_ID, notificationId);
					newIntent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
					startActivity(newIntent);
				}
			}
		}
	}

    @Override
	public void onNavigationDrawerItemSelected(MenuItemChild menuItemType) {
		final FragmentManager fragmentManager = this.getFragmentManager();
		fragment = fragmentManager.findFragmentByTag(menuItemType.toString());
		if (fragment == null) {
			// update the main content by replacing fragments
			if (menuItemType == MenuItemChild.ACCOUNT) {
				Intent intent = new Intent(this, DetailActivity.class);
				CurrentUser currentUser = CurrentUser.getCurrentUser();
				Bundle bundle = ActivityBundleFactory.getBundle(currentUser.getUserId(), currentUser, ViewType.USER_ACCOUNT);
				intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
				startActivity(intent);
			} else {
				switch (menuItemType) {
					case ALERT_KEYWORDS:
						this.mTitle = this.getResources().getString(R.string.action_bar_alert_keywords);
						fragment = AlertKeywordsListFragment.newInstance();
						break;
					case REQUESTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_pending_request);
						fragment = AccountPendingRequestsListFragment.newInstance();
						break;
					case MESSAGES:
						this.mTitle = this.getResources().getString(R.string.action_bar_message);
						fragment = ConversationsListFragment.newInstance();
						break;
					case MY_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_my_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.MY_SALE_ITEM);
						break;
					case MY_OPEN_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_my_open_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.MY_OPEN_SALE_ITEM);
						break;
					case MY_PENDING_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_my_pending_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.MY_PENDING_SALE_ITEM);
						break;
					case MY_SOLD_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_my_sold_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.MY_SOLD_SALE_ITEM);
						break;
					case MY_REJECTED_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_my_rejected_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.MY_REJECTED_SALE_ITEM);
						break;
					case ALL_OPEN_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_all_open_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.ALL_OPEN_SALE_ITEM);
						break;
					case ALL_PENDING_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_all_pending_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.ALL_PENDING_SALE_ITEM);
						break;
					case ALL_SOLD_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_all_sold_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.ALL_SOLD_SALE_ITEM);
						break;
					case ALL_REJECTED_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_all_rejected_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.ALL_REJECTED_SALE_ITEM);
						break;
					case SPONSORED_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_sponsored_posts);
						fragment = SponsoredSaleItemsFragment.newInstance();
						break;
					case SETTINGS:
						this.mTitle = this.getResources().getString(R.string.action_bar_setting);
						fragment = SettingFragment.newInstance();
						break;
					case ALL_POSTS:
						this.mTitle = this.getResources().getString(R.string.action_bar_all_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.ALL_POSTS);
						break;
					default:
						this.mTitle = this.getResources().getString(R.string.action_bar_all_posts);
						fragment = SaleItemsFragment.newInstance(SaleItemViewType.ALL_OPEN_SALE_ITEM);
						break;
				}
				fragmentManager.beginTransaction().replace(R.id.container, fragment, menuItemType.toString()).commit();
			}
		}
	}

	public void restoreActionBar() {
		final ActionBar actionBar = this.getSupportActionBar();
		//actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(this.mTitle);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!this.mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
//			this.getMenuInflater().inflate(R.menu.main, menu);
			this.restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		boolean result = super.onOptionsItemSelected(item);
		if (item.getItemId() == R.id.action_search) {
			Intent intent = new Intent(this, SearchActivity.class);
			startActivity(intent);
			overridePendingTransition(0, 0);
			result = true;
		}
		return result;
	}
	
	@Override
	public void onBackPressed() {
		if (this.mNavigationDrawerFragment.isDrawerOpen()) {
			this.mNavigationDrawerFragment.closeDrawer();
		} else {
			super.onBackPressed();
		}
	}
	
	public NavigationDrawerFragment getNavigationDrawerFragment() {
		return mNavigationDrawerFragment;
	}
	
//	/**
//	 * Class used to play sound on some events of pull down to refresh.
//	 */
//	private class SoundPlayback implements OnPullEventListener {
//
//		private SoundPool soundPool;
//		private int soundIdRefreshing;
//		private int soundIdReset;
//		
//		public SoundPlayback(Context context) {
//		    soundPool = new SoundPool(2, AudioManager.STREAM_NOTIFICATION, 0);
//		    soundIdRefreshing = soundPool.load(context, R.raw.sound_pull_down_view_refreshing, 1);
//		    soundIdReset = soundPool.load(context, R.raw.sound_pull_down_reset, 1);
//		}
//		
//		@Override
//		public void onPullEvent(PullToRefreshBase refreshView, State state) {
//			if (state == State.REFRESHING) {
//				soundPool.play(soundIdRefreshing, 1f, 1f, 1, 0, 1f);
//			} else if(state == State.RESET) {
//				soundPool.play(soundIdReset, 1f, 1f, 1, 0, 1f);
//			}
//		}
//		
//	}
}
