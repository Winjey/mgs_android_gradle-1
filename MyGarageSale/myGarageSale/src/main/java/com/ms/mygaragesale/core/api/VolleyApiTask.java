package com.ms.mygaragesale.core.api;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

import android.content.Context;

import com.android.volley.Request.Method;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.CachedJsonObjectRequest;
import com.android.volley.toolbox.CachedJsonObjectRequest.CachedJsonObject;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ms.mygaragesale.core.Logger;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.MGSError.MGSErrorType;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.response.Response;

public class VolleyApiTask<T extends Response> extends ApiTask<T> implements Listener<CachedJsonObject>, ErrorListener {
	
	public VolleyApiTask(Context context, String url, APIMethod method, Class<T> responseObjectClass, IApiTaskResponseHandler<T> apiTaskResponseHandler) {
		super(context, url, method, responseObjectClass, apiTaskResponseHandler);
	}
	
	@Override
	public void executeRequest(Object requestObject, boolean shouldResultCachedResponse) {
		System.out.println(new Gson().toJson(requestObject));
		JSONObject jsonObject = null;
		if (requestObject != null) {
			try {
				jsonObject = new JSONObject(new Gson().toJson(requestObject));
			} catch(Exception ex) {
				if (Logger.DEBUG) {
					ex.printStackTrace();
				}
			}
		}
		
		Logger.log("request: " + url);
		
		CachedJsonObject cachedJsonObject = new CachedJsonObject(shouldResultCachedResponse, jsonObject); 
		CachedJsonObjectRequest cachedJsonObjectRequest = new CachedJsonObjectRequest(this.getMethodType(), 
				url, cachedJsonObject, this, this) {
			// adding Authorization header
			public java.util.Map<String,String> getHeaders() throws com.android.volley.AuthFailureError {
				Map<String, String> headers = new HashMap<>();
				headers.put("Authorization", CurrentUser.getCurrentUser().getAccessToken());
				return headers;
			}
		};
		
//		JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
//				this.getMethodType(), this.url, jsonObject, this, this) {
			
			// adding Authorization header
//			public java.util.Map<String,String> getHeaders() throws com.android.volley.AuthFailureError {
//				Map<String, String> headers = new HashMap<>();
//				headers.put("Authorization", CurrentUser.getCurrentUser().getAccessToken());
//				return headers;
//			}
//		};
		
		cachedJsonObjectRequest.setTag(getTag());
		VolleyRequestSingleton.getInstance(this.context).addToRequestQueue(cachedJsonObjectRequest);	
	}
	
	@Override
	public void uploadFileRequest(File[] files) {
		throw new IllegalArgumentException("Please dont use this method to upload images.");
	}
	
	public void cancel() {
		VolleyRequestSingleton.getInstance(context).cancelRequest(getTag());
	}
	
	private int getMethodType() {
		switch (this.method) {
			case POST:
				return Method.POST;
			case GET:
				return Method.GET;
			case PUT:
				return Method.PUT;
			case DELETE:
				return Method.DELETE;
			default:
				return Method.GET;
		}
	}

	@Override
	public void onErrorResponse(VolleyError error) {
		Logger.log("api error: " + error.getMessage());
		if (this.apiTaskResponseHandler != null) {
			this.apiTaskResponseHandler.onApiFailure(getError(error));
		}
	}

	@Override
	public void onResponse(CachedJsonObject cachedJsonResponse) {
		try {
			Logger.log("response: "+ cachedJsonResponse.getJsonObject().toString());
			Logger.log("request response cached=" + cachedJsonResponse.isCachedResponse());
			Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
			T response = gson.fromJson(cachedJsonResponse.getJsonObject().toString(), this.responseObjectClass);
			if (response.getErrorCode() > 0) {
				// remove this key or url from cache if there is any api error
				VolleyRequestSingleton.getInstance(context).getRequestQueue().getCache().remove(url);
				if (this.apiTaskResponseHandler != null) {
					this.apiTaskResponseHandler.onApiFailure(getError(response.getErrorCode(), response.getErrorMessage()));
				}
			} else {
				if (this.apiTaskResponseHandler != null) {
					this.apiTaskResponseHandler.onApiSuccess(response, cachedJsonResponse.isCachedResponse());
				}
			}
		} catch(Exception e) {
			// remove this key or url from cache if there is any api error
			VolleyRequestSingleton.getInstance(context).getRequestQueue().getCache().remove(url);
			if (this.apiTaskResponseHandler != null) {
				this.apiTaskResponseHandler.onApiFailure(getError(0, null));
			}
		}
	}
	
	private MGSError getError(int errorCode, String errorMessage) {
		MGSError error = null;
		if (errorCode == 401) {
			error = new MGSError(MGSErrorType.UNAUTHORIZED_ERROR, errorMessage);
		} else if (errorCode == 1001) {
			error = new MGSError(MGSErrorType.USER_ACCOUNT_BLOCKED, errorMessage);
		} else if (errorCode == 409) {
			error = new MGSError(MGSErrorType.CONFLICT_ERROR, errorMessage);
		} else {
			error = new MGSError(MGSErrorType.API_ERROR, errorMessage);
		}
		return error;
	}
	
	private MGSError getError(VolleyError volleyError) {
		Logger.log("volley error: " + volleyError);
		return VolleyErrorHelper.getMessage(volleyError, this.context);
	}
	
}
