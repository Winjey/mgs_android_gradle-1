package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.ms.garagesaledeal.R;


/**
 * This view contains a edit text and button. It enable/disable button
 * with change in edit text input. Also it displays a loading view when user
 * taps on POST button.
 */
public class MessagePosterView extends LinearLayout implements TextWatcher, View.OnClickListener {

	private EditText editTextMessageText;
	private ImageButton buttonSendMessage;
	private ProgressBar progressBarSendMessage;
	private OnActionPostListener onActionPostListener;
	private boolean isInProgress;
	
	public MessagePosterView(Context context) {
		super(context);
		init(null);
	}

	public MessagePosterView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(attrs);
	}

	public MessagePosterView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(attrs);
	}
	
	@Override
	protected Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable("SUPER_STATE", super.onSaveInstanceState());
		bundle.putBoolean("IS_IN_PROGRESS", this.isInProgress);
		return bundle;
	}
	
	@Override
	protected void onRestoreInstanceState(Parcelable state) {
		if (state != null && state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			super.onRestoreInstanceState(bundle.getParcelable("SUPER_STATE"));
			this.isInProgress = bundle.getBoolean("IS_IN_PROGRESS");
			if (this.isInProgress) {
				this.progressBarSendMessage.setVisibility(View.VISIBLE);
				this.buttonSendMessage.setVisibility(View.INVISIBLE);
			} 
		} else {
			super.onRestoreInstanceState(state);
		}
	}
	
	public void setOnActionPostListener(OnActionPostListener onActionPostListener) {
		this.onActionPostListener = onActionPostListener;
	}
	
	public void requestEditViewFocus() {
		this.editTextMessageText.requestFocus();
		this.editTextMessageText.post(new Runnable() {
			@Override
			public void run() {
				final InputMethodManager inputMethodManager = (InputMethodManager) getContext()
		                .getSystemService(Context.INPUT_METHOD_SERVICE);
				inputMethodManager.showSoftInput(editTextMessageText, 0);
			}
		});
	}
	
	public void hideLoading() {
		this.isInProgress = false;
		this.progressBarSendMessage.setVisibility(View.INVISIBLE);
		this.buttonSendMessage.setVisibility(View.VISIBLE);
		this.buttonSendMessage.setEnabled(true);
	}
	
	public void reset() {
		hideLoading();
		this.editTextMessageText.setText(null);
	}
	
	public void showLoading() {
		this.isInProgress = true;
		this.progressBarSendMessage.setVisibility(View.VISIBLE);
		this.buttonSendMessage.setVisibility(View.INVISIBLE);
	}
	
	public void setHint(String hint) {
		this.editTextMessageText.setHint(hint);
	}
	
	public void setMaxHeight(int height) {
		this.editTextMessageText.setMaxHeight(height);
	}
	
	private void init(AttributeSet attributeSet) {
		inflate(getContext(), R.layout.view_message_poster, this);
		this.buttonSendMessage = (ImageButton) findViewById(R.id.button_send_message);
		this.editTextMessageText = (EditText) findViewById(R.id.edit_text_message_text);
		this.progressBarSendMessage = (ProgressBar) findViewById(R.id.progress_bar_send_message);
		this.progressBarSendMessage.setVisibility(View.INVISIBLE);
		this.editTextMessageText.addTextChangedListener(this);
		this.buttonSendMessage.setEnabled(false);
		this.buttonSendMessage.setOnClickListener(this);
	}

	@Override
	public void afterTextChanged(Editable s) {
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		String text = this.editTextMessageText.getText().toString();
		text = text.trim();
		if (text.length() > 0) {
			this.buttonSendMessage.setEnabled(true);
		} else {
			this.buttonSendMessage.setEnabled(false);
		}
	}

	@Override
	public void onClick(View view) {
		if (view == this.buttonSendMessage) {
			this.showLoading();
			String data = this.editTextMessageText.getText().toString();
			// hiding soft input
			InputMethodManager inputMethodManager = (InputMethodManager) getContext().getSystemService(
				      Context.INPUT_METHOD_SERVICE);
			inputMethodManager.hideSoftInputFromWindow(editTextMessageText.getWindowToken(), 0);
			if (this.onActionPostListener != null) {
				this.onActionPostListener.onActionPost(data);
			}
		}
	}
	
	
	/**
	 * Implements this interface to get value from edit text when user clicks on POST button.
	 */
	public interface OnActionPostListener {
		
		public abstract void onActionPost(String data);
		
	}

}
