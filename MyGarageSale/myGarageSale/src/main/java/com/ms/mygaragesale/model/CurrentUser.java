package com.ms.mygaragesale.model;

import java.io.Serializable;

import com.ms.mygaragesale.core.db.DatabaseProvider;
import com.ms.mygaragesale.core.db.dao.UserImageTable;
import com.ms.mygaragesale.core.util.StringUtil;

public class CurrentUser extends User implements Serializable {
	
 	private static final long serialVersionUID = -3854254627771203523L;
 	
 	private long id;
	private String password;
    private String accessToken;
    private UserImage userImage;
    private Location location;
    private Long userImageId;
    private Long userLocationId;
    private boolean isDataSyncd;
    
    private static CurrentUser currentUser;
	
	public static CurrentUser getCurrentUser() {
		if (currentUser == null) {
			currentUser = DatabaseProvider.getInstance().getCurrentUser();
		}
		return currentUser;
	}
	
	public void updateCurrentUser() {
		DatabaseProvider.getInstance().updateCurrentUser(currentUser);
	}
	
	public void deleteCurrentUser() {
		DatabaseProvider.getInstance().deleteCurrentUser(currentUser);
		currentUser = null;
	}

    public CurrentUser() {
    }
    
    public CurrentUser(com.ms.mygaragesale.core.db.dao.CurrentUserTable dbCurrentUser) {
    	this.id = dbCurrentUser.getId();
        this.userId = dbCurrentUser.getUserId();
        this.username = dbCurrentUser.getUsername();
        this.password = dbCurrentUser.getPassword();
        this.name = dbCurrentUser.getName();
        this.emailId = dbCurrentUser.getEmailId();
        this.mobileNumber = dbCurrentUser.getMobileNumber();
        this.address = dbCurrentUser.getAddress();
        this.location = new Location(dbCurrentUser.getUserLocationTable());
        this.accessToken = dbCurrentUser.getAccessToken();
        UserImageTable userImage = dbCurrentUser.getUserImageTable();
        if (userImage != null) {
        	this.imageId = userImage.getImageId();
		}
        this.userRole = dbCurrentUser.getUserRole();
        this.accountStatus = dbCurrentUser.getAccountStatus();
        this.accountType = dbCurrentUser.getAccountType();
        this.userImage = new UserImage(dbCurrentUser.getUserImageTable());
    }
    
    public com.ms.mygaragesale.core.db.dao.CurrentUserTable getDbCurrentUser() {
		com.ms.mygaragesale.core.db.dao.CurrentUserTable dbCurrentUser = new com.ms.mygaragesale.core.db.dao.CurrentUserTable(id, userId, 
				username, password, name, emailId, mobileNumber, address, accessToken, userRole, accountStatus, 
					accountType, isDataSyncd, userImageId, userLocationId);
		if (userImage != null) {
			dbCurrentUser.setUserImageTable(userImage.getDbUserImageTable());
		}
		if (location != null) {
			dbCurrentUser.setUserLocationTable(location.getDbUserLocationTable());
		}
		return dbCurrentUser;
    }
    
    public long getId() {
		return id;
	}
    
    public void setId(long id) {
		this.id = id;
	}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    
    public UserImage getUserImage() {
		return userImage;
	}
    
    public void setUserImage(UserImage userImage) {
		this.userImage = userImage;
	}
    
    public boolean isDataSyncd() {
		return isDataSyncd;
	}
    
    public void setDataSyncd(boolean isDataSyncd) {
		this.isDataSyncd = isDataSyncd;
	}
    
    public Location getLocation() {
		return location;
	}
    
    public void setLocation(Location location) {
		this.location = location;
	}
    
    public void copyNotNullValueWithUser(CurrentUser user) {
		if (!StringUtil.isNullOrEmpty(user.accessToken)) {
			this.accessToken = user.accessToken;
		}
		if (!StringUtil.isNullOrEmpty(user.address)) {
			this.address = user.address;
		}
		if (!StringUtil.isNullOrEmpty(user.emailId)) {
			this.emailId = user.emailId;
		}
		if (user.location != null) {
			this.location = user.location;
		}
		if (!StringUtil.isNullOrEmpty(user.mobileNumber)) {
			this.mobileNumber = user.mobileNumber;
		}
		if (!StringUtil.isNullOrEmpty(user.name)) {
			this.name = user.name;
		}
		if (!StringUtil.isNullOrEmpty(user.password)) {
			this.password = user.password;
		}
		if (!StringUtil.isNullOrEmpty(user.username)) {
			this.username = user.username;
		}
		if (user.accountStatus != 0) {
			this.accountStatus = user.accountStatus;
		}
		if (user.accountType != 0) {
			this.accountType = user.accountType;
		}
		if (user.userRole != 0) {
			this.userRole = user.userRole;
		}
		if (!StringUtil.isNullOrEmpty(user.imageId)) {
			this.imageId = user.imageId;
		}
		if (!StringUtil.isNullOrEmpty(user.userId)) {
			this.userId = user.userId;
		}
	}
    
    public double getLatitude() {
		double latitude = 0;
		if (location != null) {
			latitude = location.getLatitude();
		}
		return latitude;
    }
    
    public double getLongitude() {
    	double longitude = 0;
    	if (location != null) {
			longitude = location.getLongitude();
		}
    	return longitude;
    }

}
