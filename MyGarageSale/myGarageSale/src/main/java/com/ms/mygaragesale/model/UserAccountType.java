package com.ms.mygaragesale.model;

public class UserAccountType {
	
	private String text;
	private int accountTypeValue;
	private int index;
	
	public UserAccountType(String text, int accountTypeValue, int index) {
		super();
		this.text = text;
		this.accountTypeValue = accountTypeValue;
		this.index = index;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getAccountTypeValue() {
		return accountTypeValue;
	}
	public void setAccountTypeValue(int accountTypeValue) {
		this.accountTypeValue = accountTypeValue;
	}
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}

	
}
