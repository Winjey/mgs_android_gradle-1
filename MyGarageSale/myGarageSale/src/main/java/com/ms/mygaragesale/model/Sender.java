package com.ms.mygaragesale.model;

public class Sender {
	
	private String id;
	private String displayName;
	private boolean isModerator;
	
	public Sender() {
		// TODO Auto-generated constructor stub
	}

	public Sender(String id, String displayName, boolean isModerator) {
		super();
		this.id = id;
		this.displayName = displayName;
		this.isModerator = isModerator;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public boolean isModerator() {
		return isModerator;
	}

	public void setModerator(boolean isModerator) {
		this.isModerator = isModerator;
	}
	
	

}
