package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;

/**
 * Extended AutocCompleteTextView to provide filter results always.
 */
public class AutoCompleteTextViewEx extends AutoCompleteTextView {

	public AutoCompleteTextViewEx(Context context) {
		super(context);
	}

	public AutoCompleteTextViewEx(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public AutoCompleteTextViewEx(Context context, AttributeSet attrs,
			int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}

	@Override
	public boolean enoughToFilter() {
		return true;
	}

}
