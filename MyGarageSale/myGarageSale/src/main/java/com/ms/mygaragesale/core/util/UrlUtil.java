package com.ms.mygaragesale.core.util;


import android.net.Uri;

public class UrlUtil {
	
	private static final String BASE_URL = "http://appthisup.com/GarageSaleDeals/api/1.0/index.php/";
//	private static final String BASE_URL = "http://192.168.43.132/mgs/MyGarageSale/api/1.0/";
	private static final String GOOGLE_PLACE_API_KEY = "AIzaSyCQIt54fJO2bcPT_yDfpi3qXR9jvmjwY9Q";
//	private static final String GROUP_ID = "1416382651998653";
	
	// TODO: encode all uri string
	public static String getAddUserUrl() {
		return BASE_URL + "user";
	}
	
	public static String getEditUserUrl(String id) {
		return BASE_URL + "user/" + id;
	}
	
	public static String getUserUrl(String id) {
		return BASE_URL + "user/" + id;
	}
	
	public static String getLoginUrl() {
		return BASE_URL + "login";
	}
	
	public static String getLogoutUrl(String userId) {
		return BASE_URL + "logout/" + userId;
	}

	public static String getAddImageUrl() {
		return BASE_URL + "image";
	}
	
	public static String getImageUrl(String id) {
		return BASE_URL + "image/" + id;
	}

	public static String getAddNewSaleItem() {
		return BASE_URL + "saleItem";
	}
	
	public static String getSaleItemsByMeUrl(String userId, int saleItemStatus) {
		return BASE_URL + "saleItems/" + userId + "?itemStatus=" + saleItemStatus;
	}

    public static String getSaleItemUrl(String saleItemId) {
        return BASE_URL + "saleItem/" + saleItemId;
    }
	
	public static String getSaleItemsUrl(int saleItemStatus, double latitude, double longitude, int sponsored) {
		return BASE_URL + "saleItems" + "?itemStatus=" + saleItemStatus + 
				"&latitude=" + latitude + "&longitude=" + longitude + 
				"&sponsored=" + sponsored;
	}
	
	public static String getMarkAsSoldSaleItemUrl(String saleItemId) {
		return BASE_URL + "setSoldSaleItem/" + saleItemId;
	}
	
	public static String getActivateSaleItemUrl(String saleItemId) {
		return BASE_URL + "activateSaleItem/" + saleItemId;
	}
	
	public static String getRejectSaleItemUrl(String saleItemId) {
		return BASE_URL + "rejectSaleItem/" + saleItemId;
	}
	
	public static String getActivateUserUrl(String userId) {
		return BASE_URL + "activateUser/" + userId;
	}
	
	public static String getBlockUserUrl(String userId) {
		return BASE_URL + "blockUser/" + userId;
	}
	
	public static String getMessagesInSaleItem(String itemId) {
		return BASE_URL + "messages/" + itemId;
	}
	
	public static String getMessagesBetweenUsers(String anotherUserId) {
		return BASE_URL + "messagesBetweenUsers/" + anotherUserId;
	}
	
	public static String getSendMessageUrl() {
		return BASE_URL + "message";
	}
	
	public static String getPendingAccountsUrl() {
		return BASE_URL + "pendingAccountRequests";
	}
	
	public static String getSearchKeywordUrl(String queryStr) {
		return BASE_URL + "searchItem/" + Uri.encode(queryStr.trim());
	}
	
	public static String getConversations() {
		return BASE_URL + "conversations";
	}

	public static String getAlertKeywordsUrl() {
		return BASE_URL + "alertKeywords";
	}

	public static String getAddAlertKeywordsUrl() {
		return BASE_URL + "alertKeywords";
	}

	public static String getDeleteAlertKeywordsUrl() {
		return BASE_URL + "deleteAlertKeywords";
	}

	public static String getSaleItemUnderAlertKeywordUrl(String alertKeyword) {
		return BASE_URL + "saleItemUnderAlertKeyword/" + Uri.encode(alertKeyword.trim());
	}

	public static String getSaleItemUnderAlertKeywordsUrl() {
		return BASE_URL + "saleItemUnderAlertKeywords";
	}

//	public static String getCategoriesUrl() {
//		return BASE_URL + "categories";
//	}
//
//	public static String getAddNewCategoryUrl() {
//		return BASE_URL + "category";
//	}
//
//	public static String getDeleteCategoriesUrl(String commaSeparatedCategories) {
//		return BASE_URL + "categories/" + commaSeparatedCategories;
//	}
	
//	public static String getTagsUrl(int limit, int offset, String paginationToken) {
//		String url = BASE_URL + "tags?limit=" + limit + "&offset=" + offset;
//		if (paginationToken != null) {
//			url += "&pagination=" + paginationToken;
//		}
//		return url;
//	}
	
	public static String getLocationPredictionsUrl(String input) {
		return "https://maps.googleapis.com/maps/api/place/autocomplete/json?input="
				+ input
				+ "&types=geocode&language=en&sensor=true&key="
				+ GOOGLE_PLACE_API_KEY;
	}
	
	public static String getLocationDetailUrl(String placeId) {
		return "https://maps.googleapis.com/maps/api/place/details/json?placeid="
				+ placeId
				+ "&types=geocode&language=en&sensor=true&key="
				+ GOOGLE_PLACE_API_KEY;
	}
	
//	public static String getReadFBPostsUrl(String accessToken) {
//		return "https://graph.facebook.com/v2.2/"+ GROUP_ID +"/feed"
//				+ "?fields=id,name,message,from,application,story,updated_time,comments{from,message,created_time},"
//				+ "attachments{media,subattachments}&access_token="+ accessToken +"&format=json&sdk=android";
//	}
	
//	public static String getReadFBPostWithIdUrl(String postId, String accessToken) {
//		return "https://graph.facebook.com/v2.2/"+ postId
//				+ "?fields=id,name,message,from,application,story,updated_time,comments{from,message,created_time},"
//				+ "attachments{media,subattachments}&access_token="+ accessToken +"&format=json&sdk=android";
//	}
	
//	public static String getCommentsInFBPostUrl(String postId, String accessToken) {
//		return "https://graph.facebook.com/v2.2/"+ postId + "/comments?access_token="+ accessToken +"&format=json&sdk=android";
//	}
//
//	public static String getAttachmentsInFBPostUrl(String postId, String accessToken) {
//		return "https://graph.facebook.com/v2.2/"+ postId + "/attachments?access_token="+ accessToken +"&format=json&sdk=android";
//	}
	
//	public static String getDetailsOfFBIdUrl(String id, String accessToken) {
//		return "https://graph.facebook.com/v2.2/"+ id + "?access_token="+ accessToken +"&format=json&sdk=android";
//	}
//
//	public static String getFBPostToGroupPathComponent() {
//		return GROUP_ID + "/feed";
//	}
}
