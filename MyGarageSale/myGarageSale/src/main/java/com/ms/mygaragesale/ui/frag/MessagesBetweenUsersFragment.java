package com.ms.mygaragesale.ui.frag;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.DateUtil;
import com.ms.mygaragesale.core.util.ImageUtil;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.Message;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.request.MessageRequest;
import com.ms.mygaragesale.model.response.MessagesResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.view.MessagePosterView;
import com.ms.mygaragesale.ui.view.MessagePosterView.OnActionPostListener;

public class MessagesBetweenUsersFragment extends RefreshableItemsFragment<Message> 
			implements OnClickListener, OnActionPostListener {
	
	private MessagePosterView messagePosterView;
	
	private ApiTask<MessagesResponse> messageApiTask;
	private ApiTask<Message> sendMessageApiTask;
	
	private String userId;
	private User anotherUser;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.setRetainInstance(true);
    	
    	Bundle bundle = getArguments();
    	userId = bundle.getString(ActivityBundleFactory.KEY_USER_ID);
    	anotherUser = (User) bundle.getSerializable(ActivityBundleFactory.KEY_USER_OBJECT);
    	getMessageFromApi(true);
    }
    
    @Override
	protected void initialize() {
    	super.initialize();
    	
		messagePosterView = new MessagePosterView(getActivity());
		messagePosterView.setOnActionPostListener(this);
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
		messagePosterView.setLayoutParams(layoutParams);
		layoutOverlay.addView(messagePosterView);
				
		LayoutParams viewSwitcherParams = (RelativeLayout.LayoutParams) viewSwitcher.getLayoutParams();
		viewSwitcherParams.setMargins(0, 0, 0, SizeUtil.getSizeInPixels(50));
		
		update();
	}
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (messageApiTask != null) {
			messageApiTask.cancel();
		}
    	if (sendMessageApiTask != null) {
			sendMessageApiTask.cancel();
		}
    }
    
    @Override
    protected void update() {
    	if (messageApiTask == null && getActivity() != null) {
    		super.update();
    		if (error == null) {
				((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(anotherUser.getName() + " (" + listItems.size() + ")");
			}
    	}
    }
    
    @Override
    protected long getLastUpdatedTime() {
    	return 0;
    }
    
    @Override
    protected AbsListView getAbsListView() {
    	LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	return (AbsListView) layoutInflater.inflate(R.layout.list_view_messages, null);
    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
    	super.onActivityCreated(savedInstanceState);
    	((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(anotherUser.getName());
    }

	@Override
	protected View getItemView(int position, Message item, View convertView, ViewGroup parent) {
		final Message currentMessage = listItems.get(position);
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.view_message_items, parent, false);
		}
		TextView text_view_message_sender_name = (TextView) convertView.findViewById(R.id.text_view_message_sender_name);
		TextView text_view_message_date = (TextView) convertView.findViewById(R.id.text_view_message_date);
		TextView text_view_message = (TextView) convertView.findViewById(R.id.text_view_message);
		ImageView image_view_message_sender_icon = (ImageView) convertView.findViewById(R.id.image_view_message_sender_icon);
		
		if (currentMessage.getSender().getUserId().equals(CurrentUser.getCurrentUser().getUserId())) {
			text_view_message_sender_name.setText("Me");
		} else {
			text_view_message_sender_name.setText(currentMessage.getSender().getName());
		}
		
		text_view_message_date.setText(DateUtil.getDateTimeText(currentMessage.getCreatedDate()));
		text_view_message.setText(currentMessage.getMessage());
		
		loadUserIcon(currentMessage, image_view_message_sender_icon);
		
//		image_view_message_sender_icon.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				User user = currentMessage.getSender();
//				Intent intent = new Intent(getActivity(), DetailActivity.class);
//				Bundle bundle = ActivityBundleFactory.getBundle(user.getUserId(), user, ViewType.USER_ACCOUNT);
//				intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
//				startActivity(intent);
//			}
//		});
		
		return convertView;
	}
	
	private void loadUserIcon(Message message, final ImageView imageView) {
		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
		imageLoader.get(UrlUtil.getImageUrl(message.getSender().getImageId()), 
			new ImageLoader.ImageListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if (getActivity() != null) {
						Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
						imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
								SizeUtil.getSizeInPixels(50), bitmap));
					}
				}
				
				@Override
				public void onResponse(ImageContainer response, boolean isImmediate) {
					if (getActivity() != null) {
						Bitmap bitmap = response.getBitmap();
						if (bitmap == null) {
							bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
						}
						imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
								SizeUtil.getSizeInPixels(50), bitmap));
					}
				}
			}
		);
	}
		
	private void getMessageFromApi(boolean shouldResultCacheResponse) {
		String url = null;
		url = UrlUtil.getMessagesBetweenUsers(userId);
		messageApiTask = new ApiTaskFactory<MessagesResponse>().newInstance(getActivity(), 
				url, APIMethod.GET, MessagesResponse.class, new GetMessagesResponseHandler());
		messageApiTask.executeRequest(null, shouldResultCacheResponse);
	}
	
	private class GetMessagesResponseHandler extends ApiResponseHandler<MessagesResponse> {
		
		@Override
		public void onApiSuccess(MessagesResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			messageApiTask = null;
			MessagesBetweenUsersFragment.this.error = null;
			if (isCachedResponse) {
				swipeRefreshLayout.setRefreshing(true);
			} else {
				swipeRefreshLayout.setRefreshing(false);
			}
			
			listItems.clear();
			if (response.getMessages()!= null) {
				listItems.addAll(response.getMessages());
			}
			update();
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			messageApiTask = null;
			swipeRefreshLayout.setRefreshing(false);
			MessagesBetweenUsersFragment.this.error = error;
			update();
		}
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		showView(ContentViewType.CONTENT_LOADING);
		getMessageFromApi(true);
	}
	
	@Override
	public void onRefresh() {
		super.onRefresh();
		getMessageFromApi(false);
	}

	@Override
	public void onActionPost(String data) {
		MessageRequest message = new MessageRequest(data, null, null, Message.MESSAGE_IN_SALE_ITEM, 
				CurrentUser.getCurrentUser().getUserId(), userId);
		sendMessageApiTask = new ApiTaskFactory<Message>().newInstance(getActivity(), 
				UrlUtil.getSendMessageUrl(), APIMethod.POST, Message.class, 
					new SendMessageResponseHandler());
		sendMessageApiTask.executeRequest(message, false);
	}

	@Override
	protected void displayNoItemsMessage() {
		messageView.showMessage("No Messages Found", R.drawable.ic_message_grey600_48dp, false);
	}

	private class SendMessageResponseHandler extends ApiResponseHandler<Message> {
		
		@Override
		public void onApiSuccess(Message response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			if (getActivity() != null) {
				messagePosterView.reset();
				listItems.add(0, response);
				absListView.smoothScrollToPosition(0);
				update();
			}
			MGSApplication.showToast("Message Sent.", Toast.LENGTH_LONG);
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			MGSApplication.showToast(error.getMesssage(), Toast.LENGTH_LONG);
			if (getActivity() != null) {
				messagePosterView.hideLoading();
			}
		}
	}

}
