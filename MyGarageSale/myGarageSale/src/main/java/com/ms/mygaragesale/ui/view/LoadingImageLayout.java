package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class LoadingImageLayout extends FrameLayout {

	public LoadingImageLayout(Context context) {
		super(context);
	}
	
	public LoadingImageLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public LoadingImageLayout(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
	}
}
