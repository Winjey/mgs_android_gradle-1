package com.ms.mygaragesale.model.response;

import java.util.ArrayList;

import com.ms.mygaragesale.model.Message;

public class MessagesResponse extends Response {

	private int totalResults;
	private int limit;
	private int offset;
	private ArrayList<Message> messages;
	
	public MessagesResponse() {
		// TODO Auto-generated constructor stub
	}
	
	public MessagesResponse(int totalResults, int limit, int offset,
			ArrayList<Message> messages) {
		super();
		this.totalResults = totalResults;
		this.limit = limit;
		this.offset = offset;
		this.messages = messages;
	}



	public int getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public ArrayList<Message> getMessages() {
		return messages;
	}
	public void setMessages(ArrayList<Message> messages) {
		this.messages = messages;
	}
	
	
	
}
