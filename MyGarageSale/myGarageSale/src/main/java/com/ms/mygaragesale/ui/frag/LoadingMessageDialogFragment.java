package com.ms.mygaragesale.ui.frag;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ViewSwitcher;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.MGSApplication;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.ui.view.LoadingView;
import com.ms.mygaragesale.ui.view.MessageView;
import com.ms.mygaragesale.ui.view.MessageView.OnMessageViewTapToRetryListener;

public class LoadingMessageDialogFragment extends DialogFragment implements OnMessageViewTapToRetryListener {
	
	private ViewSwitcher viewSwitcher;
	protected LoadingView loadingView;
	protected MessageView messageView;
	protected DialogFragmentCompleteListener dialogFragmentCompleteListener;
	protected boolean isProcessStarted;
	
	protected boolean isDialogDismissed;
	
	public static LoadingMessageDialogFragment newInstance() {
		return new LoadingMessageDialogFragment();
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View view = inflater.inflate(R.layout.fragment_loading_message, null);
		viewSwitcher = (ViewSwitcher) view.findViewById(R.id.view_switcher);
		loadingView = (LoadingView) view.findViewById(R.id.loading_view);
		messageView = (MessageView) view.findViewById(R.id.message_view);
		Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(view);
		initView();
		Dialog dialog = builder.show();
		setDialogSize(dialog);
		return dialog;
	}
	
	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		isDialogDismissed = true;
	}
	
	public void setDialogFragmentCompleteListener(
			DialogFragmentCompleteListener dialogFragmentCompleteListener) {
		this.dialogFragmentCompleteListener = dialogFragmentCompleteListener;
	}
	
	protected void initView() {
		messageView.setOnMessageViewTapToRetryListener(this);
		this.loadingView.showLoadingMesage();
	}
	
	protected void showView(ContentViewType viewType) {
		viewSwitcher.setDisplayedChild(viewType.ordinal());
	}
	
	private void setDialogSize(Dialog dialog) {
		Context appContext = MGSApplication.getInstance().getApplicationContext();
		int smallestWidthDp = appContext.getResources().getConfiguration().smallestScreenWidthDp;
		int widthDp = smallestWidthDp;
		widthDp = Math.min(widthDp, SizeUtil.getSizeInPixels(320));
		int heightDp = (int)(widthDp * 3 / 4);
		dialog.getWindow().setLayout(SizeUtil.getSizeInPixels(widthDp), SizeUtil.getSizeInPixels(heightDp));
	}
	
	protected enum ContentViewType {
		LOADING, MESSAGE
	}

	@Override
	public void onTapToRetry() {
		showView(ContentViewType.LOADING);
	}
	
	public interface DialogFragmentCompleteListener {

		void onComplete(MGSError error);
		
	}
}
