package com.ms.mygaragesale.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.LocationPrediction;
import com.ms.mygaragesale.model.google.GPlace;
import com.ms.ui.view.MaterialAutoCompleteTextView;

import java.util.ArrayList;

public class LocationPredictionView extends RelativeLayout implements OnItemClickListener {
	
	private MaterialAutoCompleteTextView textView;
	private ProgressBar progressBar;
	private ImageView imageViewPoweredByGoogle;
	
	private ArrayAdapter<GPlace> locationPredictionAdapter;
	private ArrayList<GPlace> places;
	private ArrayList<GPlace> prefixedPlaces;
	private GPlace selectedPlace;
	/** Used to store previous keyword on which filter is updated to prevent multiple calls to refresh filter for same keyword which somtimes may create stack overflow. */
	private String previousKeyword;
	
	public LocationPredictionView(Context context) {
		super(context);
		this.init();
	}

	public LocationPredictionView(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.init();
	}

	public LocationPredictionView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.init();
	}
	
	@Override
	public void setEnabled(boolean enabled) {
		super.setEnabled(enabled);
		this.textView.setEnabled(enabled);
		if (enabled) {
			imageViewPoweredByGoogle.setVisibility(View.VISIBLE);
		} else {
			imageViewPoweredByGoogle.setVisibility(View.GONE);
		}
	}
	
	public GPlace getPlace() {
		if (selectedPlace != null) {
			return selectedPlace;
		} else {
			return new GPlace(this.textView.getText().toString());
		}
	}
	
	public void setPlace(GPlace place) {
		this.selectedPlace = place;
		this.textView.setText(this.selectedPlace.getDescription());
		this.textView.dismissDropDown();
	}
	
	public void setHint(String hint) {
		this.textView.setHint(hint);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		selectedPlace = prefixedPlaces.get(position);
		this.textView.setText(selectedPlace.getDescription());
	}
	
	private void init() {
		View.inflate(this.getContext(), R.layout.view_location_prediction, this);
		textView = (MaterialAutoCompleteTextView) findViewById(R.id.edit_text_place_name);
		progressBar = (ProgressBar) findViewById(R.id.progress_bar_getting_places);
		imageViewPoweredByGoogle = (ImageView) findViewById(R.id.image_view_powered_by_google);
		
		places = new ArrayList<>();
		prefixedPlaces = new ArrayList<>();
		locationPredictionAdapter = new LocationPredictionAdapter();
		textView.setAdapter(locationPredictionAdapter);
		textView.setOnItemClickListener(this);
		// this is to hide keyboard when done button is clicked during location updation.
		textView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				return false;
			}
		});
	}
	
	
	private class LocationPredictionAdapter extends ArrayAdapter<GPlace> implements Filterable {

		private ApiTask<LocationPrediction> task;
		
		public LocationPredictionAdapter() {
			super(LocationPredictionView.this.getContext(), R.layout.view_spinner_text);
		}
		
		@Override
		public int getCount() {
			return prefixedPlaces.size();
		}

		@Override
		public GPlace getItem(int position) {
			return prefixedPlaces.get(position);
		}

		@Override
		public long getItemId(int position) {
			return 0;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			TextView textView = null;
			if (convertView == null) {
				convertView = LayoutInflater.from(getContext()).inflate(R.layout.view_spinner_text, null);
				convertView.setBackgroundResource(R.drawable.selector_white_grey);
			}
			textView = (TextView) convertView;
			textView.setText(getItem(position).getDescription());
			return convertView;
		}
		
		@Override
		public Filter getFilter() {
			return new Filter() {
				
				@Override
				protected void publishResults(CharSequence constraint, FilterResults results) {
					prefixedPlaces.clear();
					if (results.count > 0 && results != null && constraint != null) {
						for (GPlace place : places) {
							if (place.getDescription().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
								if (!prefixedPlaces.contains(place)) {
									prefixedPlaces.add(place);
								}
							}
						}
						notifyDataSetChanged();
					} else {
						notifyDataSetInvalidated();
					}
				}
				
				@Override
				protected FilterResults performFiltering(CharSequence constraint) {
					FilterResults filterResults = new FilterResults();
					if (constraint != null) {
						// start API call
						String url = UrlUtil.getLocationPredictionsUrl(constraint.toString());
						if (task != null) {
							task.cancel();
						}
						task = new ApiTaskFactory<LocationPrediction>()
								.newInstance(getContext(), url, APIMethod.GET, LocationPrediction.class,
										new LocationPredictionResponseHandler());
						task.executeRequest(null, true);
					}
					

					filterResults.count = places.size();
					filterResults.values = places;
					
					return filterResults;
				}
			};
		}
	}
	
	
	private class LocationPredictionResponseHandler extends ApiResponseHandler<LocationPrediction> {
		
		@Override
		public void onApiSuccess(LocationPrediction response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			if (response != null && response.getPredictions() != null) {
				places.addAll(response.getPredictions());
				// Force the adapter to filter itself, necessary to show new data.
				// Filter based on the current text because api call is asynchronous.
				// This call will create loop hence set this filter for same keyword only once.
				if (textView.getText() != null) {
					String newKeyword = textView.getText().toString();
					if (!newKeyword.equalsIgnoreCase(previousKeyword)) {
						previousKeyword = newKeyword;
						locationPredictionAdapter.getFilter().filter(newKeyword, null);
					}
				}
			}
		}
	}
}
