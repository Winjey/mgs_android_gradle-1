package com.ms.mygaragesale.core.db.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

import de.greenrobot.dao.AbstractDao;
import de.greenrobot.dao.Property;
import de.greenrobot.dao.internal.DaoConfig;

import com.ms.mygaragesale.core.db.dao.SearchTable;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT.
/** 
 * DAO for table SEARCH_TABLE.
*/
public class SearchTableDao extends AbstractDao<SearchTable, String> {

    public static final String TABLENAME = "SEARCH_TABLE";

    /**
     * Properties of entity SearchTable.<br/>
     * Can be used for QueryBuilder and for referencing column names.
    */
    public static class Properties {
        public final static Property QueryString = new Property(0, String.class, "queryString", true, "QUERY_STRING");
    };


    public SearchTableDao(DaoConfig config) {
        super(config);
    }
    
    public SearchTableDao(DaoConfig config, DaoSession daoSession) {
        super(config, daoSession);
    }

    /** Creates the underlying database table. */
    public static void createTable(SQLiteDatabase db, boolean ifNotExists) {
        String constraint = ifNotExists? "IF NOT EXISTS ": "";
        db.execSQL("CREATE TABLE " + constraint + "'SEARCH_TABLE' (" + //
                "'QUERY_STRING' TEXT PRIMARY KEY NOT NULL );"); // 0: queryString
    }

    /** Drops the underlying database table. */
    public static void dropTable(SQLiteDatabase db, boolean ifExists) {
        String sql = "DROP TABLE " + (ifExists ? "IF EXISTS " : "") + "'SEARCH_TABLE'";
        db.execSQL(sql);
    }

    /** @inheritdoc */
    @Override
    protected void bindValues(SQLiteStatement stmt, SearchTable entity) {
        stmt.clearBindings();
 
        String queryString = entity.getQueryString();
        if (queryString != null) {
            stmt.bindString(1, queryString);
        }
    }

    /** @inheritdoc */
    @Override
    public String readKey(Cursor cursor, int offset) {
        return cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0);
    }    

    /** @inheritdoc */
    @Override
    public SearchTable readEntity(Cursor cursor, int offset) {
        SearchTable entity = new SearchTable( //
            cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0) // queryString
        );
        return entity;
    }
     
    /** @inheritdoc */
    @Override
    public void readEntity(Cursor cursor, SearchTable entity, int offset) {
        entity.setQueryString(cursor.isNull(offset + 0) ? null : cursor.getString(offset + 0));
     }
    
    /** @inheritdoc */
    @Override
    protected String updateKeyAfterInsert(SearchTable entity, long rowId) {
        return entity.getQueryString();
    }
    
    /** @inheritdoc */
    @Override
    public String getKey(SearchTable entity) {
        if(entity != null) {
            return entity.getQueryString();
        } else {
            return null;
        }
    }

    /** @inheritdoc */
    @Override    
    protected boolean isEntityUpdateable() {
        return true;
    }
    
}
