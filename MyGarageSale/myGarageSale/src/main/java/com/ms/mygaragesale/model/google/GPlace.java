package com.ms.mygaragesale.model.google;

import com.google.gson.annotations.SerializedName;
import com.twitter.sdk.android.core.models.Place;

public class GPlace {

	@SerializedName("place_id")
	private String placeId;
	private String description;
	
	public GPlace() {
	}

	public GPlace(String description) {
		super();
		this.description = description;
	}

	public String getPlaceId() {
		return placeId;
	}

	public void setPlaceId(String placeId) {
		this.placeId = placeId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		return getPlaceId().equals(((GPlace) o).getPlaceId());
	}

	@Override
	public String toString() {
		return getDescription();
	}
}
