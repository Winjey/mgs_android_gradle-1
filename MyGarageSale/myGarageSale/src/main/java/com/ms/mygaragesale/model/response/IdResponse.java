package com.ms.mygaragesale.model.response;

public class IdResponse extends Response {
	
	private String id;
	
	public IdResponse(int errorCode, String errorMessage, String id) {
		super(errorCode, errorMessage);
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
