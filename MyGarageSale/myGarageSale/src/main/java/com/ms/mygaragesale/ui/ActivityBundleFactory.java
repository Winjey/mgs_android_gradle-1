package com.ms.mygaragesale.ui;

import android.app.Fragment;
import android.os.Bundle;

import com.ms.mygaragesale.model.BaseItem;
import com.ms.mygaragesale.model.CurrentUser;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.ui.frag.AddNewSaleItemFragment;
import com.ms.mygaragesale.ui.frag.SaleItemsUnderAlertKeywordFragment;
import com.ms.mygaragesale.ui.frag.AuthenticationFragment;
import com.ms.mygaragesale.ui.frag.MessagesBetweenUsersFragment;
import com.ms.mygaragesale.ui.frag.MessagesInConversationsFragment;
import com.ms.mygaragesale.ui.frag.MyAccountFragment;
import com.ms.mygaragesale.ui.frag.SaleItemDetailsFragment;
import com.ms.mygaragesale.ui.frag.UserAccountFragment;

/**
 * This class is used to get bundle based on given inputs, that can be passed between activities and fragments.
 */
public class ActivityBundleFactory {
	
	public static final String KEY_FRAGMENT_CLASS_NAME = "KEY_FRAGMENT_CLASS_NAME";
	public static final String KEY_ID = "KEY_ID";
	public static final String KEY_OBJECT = "KEY_OBJECT";
	
	public static final String KEY_USER_ID = "KEY_USER_ID";
	public static final String KEY_USER_OBJECT = "KEY_USER_OBJECT";
	
	public static final String KEY_SALE_ITEM_ID = "KEY_SALE_ITEM_ID";
	public static final String KEY_SALE_ITEM_OBJECT = "KEY_SALE_ITEM_OBJECT";
	
	public static final String KEY_FB_POST_ID = "KEY_FB_POST_ID";
	public static final String KEY_FB_POST_OBJECT = "KEY_FB_POST_OBJECT";
	
	public static final String KEY_CONV_ID = "KEY_CONV_ID";
	public static final String KEY_CONV_OBJECT = "KEY_CONV_OBJECT";

	/**
	 * Method to get a bundle based on inputs and fragment to be displayed.
	 * Use this method to get bundle to pass between activities and fragments.
	 * 
	 * @param uniqueId - unique id of asset, pass null if no asset id present instead of 0.
	 * @param object - BaseItem to be passed in bundle.
	 * @param viewType - Fragment type that is to be displayed. Pass ActionType.NONE when bundle for DetailFragment is required.
	 * @return - Bundle with given inputs.
	 */
	public static Bundle getBundle(String uniqueId, BaseItem object, ViewType viewType) {
		Bundle bundle = new Bundle();
		switch (viewType) {
			case MESSAGE_BETWEEN_USER:
				if (uniqueId != null) {
					bundle.putString(KEY_USER_ID, uniqueId);
				}
				bundle.putSerializable(KEY_USER_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, MessagesBetweenUsersFragment.class);
				break;
			case MESSAGE_IN_CONVERSATION:
				if (uniqueId != null) {
					bundle.putString(KEY_CONV_ID, uniqueId);
				}
				bundle.putSerializable(KEY_CONV_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, MessagesInConversationsFragment.class);
				break;
			case MESSAGE_IN_SALE_ITEM:
				if (uniqueId != null) {
					bundle.putString(KEY_CONV_ID, uniqueId);
				}
				bundle.putSerializable(KEY_SALE_ITEM_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, MessagesInConversationsFragment.class);
				break;
			case SALE_ITEM_DETAIL:
				if (uniqueId != null) {
					bundle.putString(KEY_SALE_ITEM_ID, uniqueId);
				}
				bundle.putSerializable(KEY_SALE_ITEM_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, SaleItemDetailsFragment.class);
				break;
			case USER_ACCOUNT:
				String id = null;
				if (uniqueId != null) {
					id = uniqueId;
					bundle.putString(KEY_USER_ID, uniqueId);
				}
				if (object != null) {
					id = ((User) object).getUserId();
					bundle.putSerializable(KEY_USER_OBJECT, object);
				}
				Class<? extends Fragment> clazz = UserAccountFragment.class;
				if (id != null && id.equals(CurrentUser.getCurrentUser().getUserId())) {
					clazz = MyAccountFragment.class;
				}
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, clazz);
				break;
			case CREATE_POST:
				if (uniqueId != null) {
					bundle.putString(KEY_ID, uniqueId);
				}
				bundle.putSerializable(KEY_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, AddNewSaleItemFragment.class);
				break;
			case CREATE_SPONSORED_POST:
				if (uniqueId != null) {
					bundle.putString(KEY_ID, uniqueId);
				}
				bundle.putSerializable(KEY_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, AddNewSaleItemFragment.class);
				break;
			case AUTH:
				if (uniqueId != null) {
					bundle.putString(KEY_ID, uniqueId);
				}
				bundle.putSerializable(KEY_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, AuthenticationFragment.class);
				break;
			case ALERT_KEYWORD_SALE_ITEM:
				bundle.putSerializable(KEY_OBJECT, object);
				bundle.putSerializable(KEY_FRAGMENT_CLASS_NAME, SaleItemsUnderAlertKeywordFragment.class);
			case NONE:
			default:
		}
		return bundle;
	}
	
	/**
	 * Used to display fragment using action type.
	 */
	public enum ViewType {
		
		MESSAGE_BETWEEN_USER,
		MESSAGE_IN_CONVERSATION,
		MESSAGE_IN_SALE_ITEM,
		CREATE_POST,
		SALE_ITEM_DETAIL,
		FB_POST_DETAIL,
		USER_ACCOUNT,
		AUTH,
		CREATE_SPONSORED_POST,
		ALERT_KEYWORD_SALE_ITEM,
		NONE // no action to perform, for example in case of DetailFrsagment
	}
	
}
