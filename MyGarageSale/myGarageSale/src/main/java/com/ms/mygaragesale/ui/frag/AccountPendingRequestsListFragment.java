package com.ms.mygaragesale.ui.frag;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageLoader.ImageContainer;
import com.ms.garagesaledeal.R;
import com.ms.mygaragesale.core.AppSharedPref;
import com.ms.mygaragesale.core.MGSError;
import com.ms.mygaragesale.core.api.ApiResponseHandler;
import com.ms.mygaragesale.core.api.ApiTask;
import com.ms.mygaragesale.core.api.ApiTask.APIMethod;
import com.ms.mygaragesale.core.api.ApiTaskFactory;
import com.ms.mygaragesale.core.api.VolleyRequestSingleton;
import com.ms.mygaragesale.core.util.ImageUtil;
import com.ms.mygaragesale.core.util.SizeUtil;
import com.ms.mygaragesale.core.util.UrlUtil;
import com.ms.mygaragesale.model.User;
import com.ms.mygaragesale.model.response.UsersResponse;
import com.ms.mygaragesale.ui.ActivityBundleFactory;
import com.ms.mygaragesale.ui.ActivityBundleFactory.ViewType;
import com.ms.mygaragesale.ui.DetailActivity;

public class AccountPendingRequestsListFragment extends RefreshableItemsWithCountHeaderFragment<User> 
			implements OnClickListener {
	
	private ApiTask<UsersResponse> pendingAccountsApiTask;
	
	public static AccountPendingRequestsListFragment newInstance() {
		return new AccountPendingRequestsListFragment();
	}
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.setRetainInstance(true);   	
    	getPendingAccountsListFromApi(true);
    }
    
    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int index, long id) {
    	User user = listItems.get(index - 1);
		Intent intent = new Intent(getActivity(), DetailActivity.class);
		Bundle bundle = ActivityBundleFactory.getBundle(user.getUserId(), user, ViewType.USER_ACCOUNT);
		intent.putExtra(DetailActivity.KEY_DETAIL_ACTIVITY_BUNDLE, bundle);
		startActivityForResult(intent, UserAccountFragment.REQUEST_CODE_USER_DETAIL);
    }
    
    @Override
    protected void update() {
    	if (pendingAccountsApiTask == null && getActivity() != null) {
    		super.update();
    	}
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	if (pendingAccountsApiTask != null) {
    		pendingAccountsApiTask.cancel();
    		pendingAccountsApiTask = null;
    	}
    }
    
    @Override
	protected long getLastUpdatedTime() {
		long lastUpdatedTime = 0;
		lastUpdatedTime = new AppSharedPref().getRequestListLastUpdatedTime();
		return lastUpdatedTime;
	}
	
	private void setLastUpdatedTime(long time) {
		new AppSharedPref().setRequestListLastUpdatedTime(time);
	}
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
    	if (requestCode == UserAccountFragment.REQUEST_CODE_USER_DETAIL && resultCode == Activity.RESULT_OK) {
			getPendingAccountsListFromApi(false);
		}
    }
    
    @Override
    protected AbsListView getAbsListView() {
    	LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	return (AbsListView) layoutInflater.inflate(R.layout.list_view_messages, null);
    }
    
	@Override
	protected View getItemView(int position, User user, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = layoutInflater.inflate(R.layout.view_pending_account_requests, parent, false);
		}
		TextView text_view_user_name = (TextView) convertView.findViewById(R.id.text_view_user_name);
		TextView text_view_pending_request_text = (TextView) convertView.findViewById(R.id.text_view_pending_request_text);
		ImageView image_view_user_icon = (ImageView) convertView.findViewById(R.id.image_view_user_icon);
		
		text_view_user_name.setText(user.getName());
		text_view_pending_request_text.setText(getPendingRequestText(user.getAccountStatus()));
		loadUserIcon(user, image_view_user_icon);
				
		return convertView;
	}

	@Override
	protected void displayNoItemsMessage() {
		messageView.showMessage("No Pending Requests", R.drawable.ic_assignment_returned_grey600_48dp, false);
	}

	private String getPendingRequestText(int accountStatus) {
		String text = null;
		if (accountStatus == User.ACCOUNT_STATUS_PENDING) {
			text =  "Requesting to activate account";
		} else if (accountStatus == User.ACCOUNT_STATUS_UPGRADE_PENDING) {
			text =  "Requesting to upgrade account";
		} else if (accountStatus == User.ACCOUNT_STATUS_DOWNGRADE_PENDING) {
			text =  "Requesting to downgrade account";
		}
		return text;
	}
	
	private void loadUserIcon(User user, final ImageView imageView) {
		ImageLoader imageLoader = VolleyRequestSingleton.getInstance(getActivity()).getImageLoader();
		imageLoader.get(UrlUtil.getImageUrl(user.getImageId()), 
			new ImageLoader.ImageListener() {
				@Override
				public void onErrorResponse(VolleyError error) {
					if (getActivity() != null) {
						Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
						imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
								SizeUtil.getSizeInPixels(50), bitmap));
					}
				}
				
				@Override
				public void onResponse(ImageContainer response, boolean isImmediate) {
					if (getActivity() != null) {
						Bitmap bitmap = response.getBitmap();
						if (bitmap == null) {
							bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.avatar_blue_120);
						}
						imageView.setImageBitmap(ImageUtil.getRoundedShape(SizeUtil.getSizeInPixels(50), 
								SizeUtil.getSizeInPixels(50), bitmap));
					}
				}
			}
		);
	}
		
	private void getPendingAccountsListFromApi(boolean shouldResultCacheResponse) {
		String url = null;
		url = UrlUtil.getPendingAccountsUrl();
		pendingAccountsApiTask = new ApiTaskFactory<UsersResponse>().newInstance(getActivity(), 
				url, APIMethod.GET, UsersResponse.class, new GetPendingAccountRequestsResponseHandler());
		pendingAccountsApiTask.executeRequest(null, shouldResultCacheResponse);
	}
	
	private class GetPendingAccountRequestsResponseHandler extends ApiResponseHandler<UsersResponse> {
		
		@Override
		public void onApiSuccess(UsersResponse response, boolean isCachedResponse) {
			super.onApiSuccess(response, isCachedResponse);
			pendingAccountsApiTask = null;
			AccountPendingRequestsListFragment.this.error = null;
			
			if (!isCachedResponse) {
				setLastUpdatedTime(System.currentTimeMillis());
				swipeRefreshLayout.setRefreshing(false);
			} else {
				swipeRefreshLayout.setRefreshing(true);
			}
						
			listItems.clear();
			if (response.getUsers() != null) {
				listItems.addAll(response.getUsers());
			}
			update();
		}
		
		@Override
		public void onApiFailure(MGSError error) {
			super.onApiFailure(error);
			pendingAccountsApiTask = null;
			swipeRefreshLayout.setRefreshing(false);
			AccountPendingRequestsListFragment.this.error = error;
			update();
		}
	}
	
	@Override
	public void onTapToRetry() {
		super.onTapToRetry();
		getPendingAccountsListFromApi(true);
	}
	
	@Override
	protected void setItemsCountTitle() {
		if (textViewItemsCount != null) {
			textViewItemsCount.setText(getResources().getQuantityString(
					R.plurals.pending_request_count_plural, listItems.size(),
					listItems.size()));
		}
	}
	
	@Override
	public void onRefresh() {
		super.onRefresh();
		getPendingAccountsListFromApi(false);
	}

}
