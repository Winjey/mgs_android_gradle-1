package com.ms.mygaragesale;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class MGSDaoCreator {

	public static void main(String[] args) throws Exception {
		MGSDaoCreator creator = new MGSDaoCreator();
//		creator.createSchemaVersion2();
		creator.createSchemaVersion3();
	}
		
	private void createSchemaVersion2() throws Exception {
		Schema schema = new Schema(2, "com.ms.mygaragesale.core.db.dao");
		
		Entity entityUserImage = schema.addEntity("UserImageTable");
		entityUserImage.addIdProperty();
		entityUserImage.addStringProperty("imageId");
		entityUserImage.addStringProperty("imageUri");
		
		Entity entityUserLocation = schema.addEntity("UserLocationTable");
		entityUserLocation.addIdProperty();
		entityUserLocation.addStringProperty("name");
		entityUserLocation.addDoubleProperty("latitude");
		entityUserLocation.addDoubleProperty("longitude");
		
		Entity entityUser = schema.addEntity("CurrentUserTable");
		entityUser.addIdProperty();
		entityUser.addStringProperty("userId");
		entityUser.addStringProperty("username");
		entityUser.addStringProperty("password");
		entityUser.addStringProperty("name");
		entityUser.addStringProperty("emailId");
		entityUser.addStringProperty("mobileNumber");
		entityUser.addStringProperty("address");
		entityUser.addStringProperty("accessToken");
		entityUser.addIntProperty("userRole");
		entityUser.addIntProperty("accountStatus");
		entityUser.addIntProperty("accountType");
		entityUser.addBooleanProperty("isDataSyncd");
		Property entityUserImageId = entityUser.addLongProperty("userImageId").getProperty();
		entityUser.addToOne(entityUserImage, entityUserImageId);
		Property entityUserLocationId = entityUser.addLongProperty("userLocationId").getProperty();
		entityUser.addToOne(entityUserLocation, entityUserLocationId);
		
		Entity entityCategory = schema.addEntity("CategoryTable");
		entityCategory.addIdProperty();
		entityCategory.addStringProperty("categoryName");
		
		Entity entitySearch = schema.addEntity("SearchTable");
		entitySearch.addIdProperty();
		entitySearch.addStringProperty("queryString");
		
		new DaoGenerator().generateAll(schema, "gen_dao");
	}
	
	private void createSchemaVersion3() throws Exception {
		Schema schema = new Schema(3, "com.ms.mygaragesale.core.db.dao");
		
		Entity entityUserImage = schema.addEntity("UserImageTable");
		entityUserImage.addIdProperty();
		entityUserImage.addStringProperty("imageId");
		entityUserImage.addStringProperty("imageUri");
		
		Entity entityUserLocation = schema.addEntity("UserLocationTable");
		entityUserLocation.addIdProperty();
		entityUserLocation.addStringProperty("name");
		entityUserLocation.addDoubleProperty("latitude");
		entityUserLocation.addDoubleProperty("longitude");
		
		Entity entityUser = schema.addEntity("CurrentUserTable");
		entityUser.addIdProperty();
		entityUser.addStringProperty("userId");
		entityUser.addStringProperty("username");
		entityUser.addStringProperty("password");
		entityUser.addStringProperty("name");
		entityUser.addStringProperty("emailId");
		entityUser.addStringProperty("mobileNumber");
		entityUser.addStringProperty("address");
		entityUser.addStringProperty("accessToken");
		entityUser.addIntProperty("userRole");
		entityUser.addIntProperty("accountStatus");
		entityUser.addIntProperty("accountType");
		entityUser.addBooleanProperty("isDataSyncd");
		Property entityUserImageId = entityUser.addLongProperty("userImageId").getProperty();
		entityUser.addToOne(entityUserImage, entityUserImageId);
		Property entityUserLocationId = entityUser.addLongProperty("userLocationId").getProperty();
		entityUser.addToOne(entityUserLocation, entityUserLocationId);
		
		Entity entityCategory = schema.addEntity("CategoryTable");
		entityCategory.addIdProperty();
		entityCategory.addStringProperty("categoryName");
		
		Entity entityAlertKeyword = schema.addEntity("AlertKeywordTable");
		entityAlertKeyword.addStringProperty("keywordName").primaryKey();
		entityAlertKeyword.addIntProperty("keywordType");
		
		Entity entitySearch = schema.addEntity("SearchTable");
		entitySearch.addStringProperty("queryString").primaryKey();
		
		new DaoGenerator().generateAll(schema, "gen_dao");
	}

}
